export interface NodeShape {
  shape: any;
  points?: string;
  radius?: number;
}