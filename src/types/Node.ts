export interface Node {
  id: string;
  label?: string;
  x: number;
  y: number;
  classes?: Array<string>;
  hidden?: boolean;
  selected?: boolean;
  metadata?: {[key: string]: string | number | {[key: string]: string | number} | Array<string> | boolean | Array<number>};
}
