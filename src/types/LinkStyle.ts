export interface LinkStyle {
  display ?: boolean;
  stroke?: string;
  strokeWidth?: number;
  opacity?: number;
  dash?: boolean;
}
