import type { Node } from "./Node";
import type { Link } from "./Link";

export interface Network {
	id: String;
	label?: String;
	nodes: {
		[key: string]: Node
	};
	links: Array<Link>;
	type?: String;
	rescale?: Boolean;
}
