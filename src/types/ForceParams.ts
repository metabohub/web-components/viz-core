import type { ForceChargeParams } from "./ForceParams/ForceChargeParams";
import type { ForceCollideParams } from "./ForceParams/ForceCollideParams";
import type { ForceGravityParams } from "./ForceParams/ForceGravityParams";
import type { ForceLinkParams } from "./ForceParams/ForceLinkParams";

export interface ForceParams {
  charge: ForceChargeParams;
  collide: ForceCollideParams;
  gravity: ForceGravityParams;
  link: ForceLinkParams;
}