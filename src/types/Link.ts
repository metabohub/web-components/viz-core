import type { Node } from "./Node";

export interface Link {
  id: string;
  classes?: Array<string>;
  label?: string;
  type?: string;
  source: Node;
  target: Node;
  relation?: string;
  directed?: boolean;
  metadata?: {[key: string]: string | number | {[key: string]: string | number} | Array<string | number> | boolean};
}
