export interface NodeStyle {
  height?: number;
  width?: number;
  fill?: string;
  strokeWidth?: number;
  stroke?: string;
  displayLabel?: boolean;
  labelPosition?: string;
  label?: string;
  rx?: number;
  ry?: number;
  shape?: string;
  opacity?: number;
  fontColor?: string;
  fontSize?: number;
  fontOpacity?: number;
}