export interface ForceCollideParams {
  strength: number;
  radius: number;
  iteration: number;
}