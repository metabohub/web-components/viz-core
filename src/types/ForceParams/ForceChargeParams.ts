export interface ForceChargeParams {
  strength: number;
  min: number;
  max: number;
}