export interface ForceLinkParams {
  distance: number;
  iteration: number;
}