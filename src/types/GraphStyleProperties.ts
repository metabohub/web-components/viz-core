import { NodeStyle } from "./NodeStyle";
import { LinkStyle } from "./LinkStyle";

export interface GraphStyleProperties {
	nodeStyles?: { [key: string]: NodeStyle };
	linkStyles?: { [key: string]: LinkStyle };
	curveLine?: boolean;
	directed?: boolean;
}