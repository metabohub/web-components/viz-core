export interface Convexhulls {
  clusters: {[key: string]: Array<string>};
  selected: Array<string>;
  clustersColor: {[key: string]: string};
  display: boolean;
}
