import { Node } from "../types/Node";
import { NodeStyle } from "../types/NodeStyle";

/**
 * Retun a node style according to graph style properties
 * If the type of a node corresponds to an entry in the node styles
 * return the corresponding style, otherwise return a default style
 * @param node a Node object
 * @param styles a GraphStyleProperties.nodeStyles object
 * @returns {NodeStyle} a NodeStyle object
 */
export function getNodeStyle(node: Node, styles: {[key: string]: NodeStyle}): NodeStyle {

	const defaultNodeStyle: NodeStyle = {
		height: 30,
		width: 30,
		fill: 'white',
		strokeWidth: 2,
		stroke: '#000000',
		displayLabel: true,
		labelPosition: 'middle',
		rx: 15,
		ry: 15,
		opacity: 1,
		shape: 'circle',
		fontSize: 10,
		fontColor: '#000000',
		fontOpacity: 0.5
	};

	let mergedStyle = { ...defaultNodeStyle };

	if (node) {
		const classes = node.classes;

		classes?.forEach((type) => {
			if (styles && styles[type]) {
				mergedStyle = { ...mergedStyle, ...styles[type]};
			}
		});
	}

	return mergedStyle;

}