import { Link } from "../types/Link";
import { LinkStyle } from "../types/LinkStyle";

/**
 * Retun a link style according to graph style properties
 * If the type of a link corresponds to an entry in the link styles
 * return the corresponding style, otherwise return a default style
 * @param link a Link object
 * @param styles a GraphStyleProperties.linkStyles object
 * @returns {LinkStyle} a LinkStyle object
 */
export function getLinkStyle(link: Link, styles: {[key: string]: LinkStyle}): LinkStyle {

	const defaultLinkStyle: LinkStyle = {
		stroke: '#000000',
		strokeWidth: 2,
		opacity: 0.5,
		display: true,
		dash: false
	};
	
	let mergedStyle = { ...defaultLinkStyle };

	if (link) {
		const classes = link.classes;
	
		classes?.forEach((type) => {
			if (styles && styles[type]) {
				mergedStyle = { ...mergedStyle, ...styles[type]};
			}
		});
	}

	return mergedStyle;
};