/**
 * All the functions to manage the library
 */

/**
 * Return size [width, height] of the svg d3Viz
 * @returns {Array<number | undefined>} Array of width (0) and height (1) of the svg
 */
export function screenSize(graphSvg: SVGElement | HTMLElement): Array<number | undefined> {
  const svgWidth = graphSvg?.clientWidth;
  const svgHeight = graphSvg?.clientHeight;
  
  return [svgWidth, svgHeight];
}

/**
 * 
 * @param x X node position
 * @param y Y node position
 * @param transformMatrix PanZoom transform matrix
 * @returns {Array<number>} Real position of node over the screen
 */
export function applyInverseTransform(x: number, y: number, transformMatrix: number[]) {
  const [tx, ty, realZoom] = transformMatrix;
  const realX = (x - tx) / realZoom;
  const realY = (y - ty) / realZoom;
  
  return [realX, realY];
}

/**
 * Transform panel string for style to html attribute
 * @param panelString Panel string corresponding to specific style
 * @returns {string} html attribute for node / link
 */
export function humanToAttribute (panelString: string): string | undefined {
  if (panelString === 'Background color') {
    return 'fill' as string;
  }
  if (panelString === 'Size') {
    return 'size' as string;
  }
}

/**
 * Return specific hexadecimal code compute from compartment name
 * @param compartment Compartment name
 * @returns {string | undefined} Hexadecimal code
 */
export function stringToRGB (compartment: string | undefined): string | undefined {
  if (compartment) {
    let strLength = compartment.length;
    let sectionLength = strLength / 3;
    let roundSectionLength = Math.round(sectionLength);

    if (roundSectionLength < 1) {
        roundSectionLength = 1;
    }
    let strSection = compartment.match(new RegExp('.{1,' + roundSectionLength + '}', 'g'));

    let redSum = 0;
    let greenSum = 0;
    let blueSum = 0;

    if (strSection) {
      for (let i = 0; i < roundSectionLength; i++){
        if (strSection[0] !== undefined) {
            redSum += strSection[0].charCodeAt(i);
        }
        if (strSection[1] !== undefined) {
            greenSum += strSection[1].charCodeAt(i);
        }
        if (strSection[2] !== undefined) {
            blueSum += strSection[2].charCodeAt(i);
        }
      }
    }

    let redCode = Math.sin(redSum % 256) * 256;
    let blueCode = Math.sin(blueSum % 256) * 256;
    let greenCode = Math.sin(greenSum % 256) * 256;

    const hexColor = RGB2Color(redCode, blueCode, greenCode);
    return hexColor;
  } else {
    return undefined;
  }
}

/**
 * Transform RGB code in hexadecimal code
 * @param r Red number
 * @param g Green number
 * @param b Blue number
 * @returns {string} Hexadecimal code
 */
function RGB2Color (r: number, g: number, b: number): string {
  return '#' + byte2Hex(r) + byte2Hex(g) + byte2Hex(b);
}

/**
 * Transform a byte in hexadecimal code
 * @param n Number of byte
 * @returns {string} Hexadecimal code
 */
function byte2Hex (n: number): string {
  let nybHexString = "0123456789ABCDEF";
  return String(nybHexString.substr((n >> 4) & 0x0F,1)) + nybHexString.substr(n & 0x0F,1);
};