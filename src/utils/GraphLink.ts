/**
 * All the functions to manage links
 */

// Import -----------
// Type -----------
import type { Link } from "../types/Link";
import { NodeStyle } from "../types/NodeStyle";

//import { linePath } from "../composables/UseLinePath";
import { curvedLinePath, linePath, reversibleDirectedLinePath, directedLinePath } from "../composables/UseLinePath";

// Functions --------
/**
 * Switch between different path compute functions
 * @param source Source node
 * @param target Target node
 * @param sourceStyle Style of source node
 * @param targetStyle Style of target node
 * @returns {string | undefined} Call function to compute linePath and return string of line path
 */
export function transformAttribute(
	lineStyle: boolean,
	directed: boolean,
	source: Link['source'] | undefined,
	target: Link['target'] | undefined,
	sourceStyle: NodeStyle,
	targetStyle: NodeStyle,
	exiting: boolean,
	shift: number = undefined
): string | undefined {
	if (source?.id !== target?.id) {
		if (source?.x && source?.y && target?.x && target?.y) {
			const sourceWidth = sourceStyle.width!;
			const sourceHeight = sourceStyle.height!;
			const targetWidth = targetStyle.width!;
			const targetHeight = targetStyle.height!;

			if (lineStyle) {
				return curvedLinePath(source.x, source.y, target.x, target.y, sourceWidth, sourceHeight, targetWidth, targetHeight, exiting, shift);
			} else {
				if (directed) {
					if (source.metadata?.reversible || target.metadata?.reversible) {
						return reversibleDirectedLinePath(source.x, source.y, target.x, target.y, sourceWidth, sourceHeight, targetWidth, targetHeight);
					} else {
						return directedLinePath(source.x, source.y, target.x, target.y, sourceWidth, sourceHeight, targetWidth, targetHeight);
					}
				} else {
					return linePath(source.x, source.y, target.x, target.y, sourceWidth, sourceHeight, targetWidth, targetHeight);
				}
			}
			
		}
	} else {
		return undefined;
	}
}
