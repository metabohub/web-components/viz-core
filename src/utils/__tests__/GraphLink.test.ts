import { expect, test, describe } from 'vitest';

import { transformAttribute } from '../GraphLink';

const sourceStyle = {
  height: 30,
  width: 30,
  fill: 'white',
  strokeWidth: 2,
  displayLabel: true
};

const targetStyle = {
  height: 30,
  width: 30,
  fill: 'white',
  strokeWidth: 2,
  displayLabel: true
};

describe('Test GraphLink utils', () => {
  test('transformAttribute function', () => {
    const badParam = {
      source: undefined,
      target: undefined,
      sourceStyle: sourceStyle,
      targetStyle: targetStyle
    };

    // const badPath = transformAttribute(
    //   false,
    //   false,
    //   badParam.source, 
    //   badParam.target, 
    //   badParam.sourceStyle, 
    //   badParam.targetStyle
    // );

    // expect(badPath).toBeUndefined();

    // const sameNodeParam = {
    //   source: {
    //     type: 'metabolite',
    //     id: 'Acetyl',
    //     label: 'Acetyl',
    //     x: 10,
    //     y: 320,
    //     metadata: {}
    //   },
    //   target: {
    //     type: 'metabolite',
    //     id: 'Acetyl',
    //     label: 'Acetyl',
    //     x: 10,
    //     y: 320,
    //     metadata: {}
    //   },
    //   sourceStyle: sourceStyle,
    //   targetStyle: targetStyle
    // };

    // const sameNodePath = transformAttribute(
    //   false,
    //   false,
    //   sameNodeParam.source, 
    //   sameNodeParam.target, 
    //   sameNodeParam.sourceStyle, 
    //   sameNodeParam.targetStyle
    // );

    // expect(sameNodePath).toBeUndefined();

    // const goodParam = {
    //   source: {
    //     type: 'metabolite',
    //     id: 'Acetyl',
    //     label: 'Acetyl',
    //     x: 10,
    //     y: 10,
    //     metadata: {}
    //   },
    //   target: {
    //     type: 'reaction',
    //     id: 'Polymerase',
    //     label: 'Polymerase',
    //     x: 15,
    //     y: 15,
    //     metadata: {}
    //   },
    //   sourceStyle: sourceStyle,
    //   targetStyle: targetStyle
    // };

    // const goodPath = transformAttribute(
    //   false,
    //   false,
    //   goodParam.source,
    //   goodParam.target,
    //   goodParam.sourceStyle,
    //   goodParam.targetStyle
    // );

    // const pathResponse = `M25,25L30,30`;

    // expect(goodPath).toBeDefined();
    // expect(goodPath).toBe(pathResponse);
  });
});