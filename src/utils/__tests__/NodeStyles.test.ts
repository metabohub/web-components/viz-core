import { expect, test, describe } from 'vitest';
import { getNodeStyle } from '../NodeStyles';

const node1 = {
  id: 'Acetyl',
  label: 'Acetyl',
  classes: ['reaction'],
  metadata: {},
  x: 0,
  y: 0
}

const node2 = {
  id: 'Acetyl',
  label: 'Acetyl',
  classes: ['metabolite'],
  metadata: {},
  x: 0,
  y: 0
}

const node3 = {
  id: 'Acetyl',
  label: 'Acetyl',
  classes: ['metabolite', 'circle'],
  metadata: {},
  x: 0,
  y: 0
}

const GraphStyleProperties = {
  nodeStyles: {
    'metabolite': {
      height: 30,
      width: 30,
      fill: 'red',
      strokeWidth: 2,
      displayLabel: true,
      labelPosition: 'top',
    },
    'reaction': {
      height: 15,
      width: 15,
      fill: 'white',
      strokeWidth: 2,
      displayLabel: true,
    },
    'circle': {
      rx: 30,
      ry: 30,
      labelPosition: 'bottom',
    }
  },
  linkStyles: {
    // ... empty for tests
  }
}

describe('Test NodeStyles utils', () => {
  test('getNodeStyle function', () => {
    const metaboliteStyle = {
      height: 30,
      width: 30,
      fill: 'red',
      strokeWidth: 2,
      displayLabel: true,
      labelPosition: 'top',
      rx: 15,
      ry: 15,
      shape: "circle",
      opacity: 1,
      stroke: "#000000",
      fontSize: 10,
      fontColor: '#000000',
      fontOpacity: 0.5
    };

    const reactionStyle = {
      height: 15,
      width: 15,
      fill: 'white',
      strokeWidth: 2,
      displayLabel: true,
      labelPosition: 'middle',
      rx: 15,
      ry: 15,
      opacity: 1,
      shape: "circle",
      stroke: "#000000",
      fontSize: 10,
      fontColor: '#000000',
      fontOpacity: 0.5
    };

    const circleMetabolite = {
      height: 30,
      width: 30,
      fill: 'red',
      strokeWidth: 2,
      displayLabel: true,
      labelPosition: 'bottom',
      rx: 30,
      ry: 30,
      opacity: 1,
      shape: "circle",
      stroke: "#000000",
      fontSize: 10,
      fontColor: '#000000',
      fontOpacity: 0.5
    };

    const node1Style = getNodeStyle(node1, GraphStyleProperties.nodeStyles);
    const node2Style = getNodeStyle(node2, GraphStyleProperties.nodeStyles);
    const node3Style = getNodeStyle(node3, GraphStyleProperties.nodeStyles);

    expect(node1Style).toStrictEqual(reactionStyle);
    expect(node2Style).toStrictEqual(metaboliteStyle);
    expect(node3Style).toStrictEqual(circleMetabolite);
  });
});