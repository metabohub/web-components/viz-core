/**
 * @vitest-environment jsdom
 */

import { describe, test, expect, beforeEach, it } from "vitest";

import { screenSize, stringToRGB, applyInverseTransform } from "../GraphUtils";

describe('Test GraphUtils utils', () => {
  let mockDivElement: HTMLElement;

  beforeEach(() => {
    mockDivElement = document.createElement('div');
  });

  it('should correctly apply inverse transform with positive numbers', () => {
    const x = 100;
    const y = 200;
    const transformMatrix = [10, 20, 2]; // Translation (tx, ty) = (10, 20), zoom factor = 2

    const [realX, realY] = applyInverseTransform(x, y, transformMatrix);

    expect(realX).toBe((100 - 10) / 2); // Expected: (90 / 2) = 45
    expect(realY).toBe((200 - 20) / 2); // Expected: (180 / 2) = 90
  });

  it('should correctly apply inverse transform with negative translation values', () => {
    const x = 50;
    const y = 75;
    const transformMatrix = [-10, -20, 1.5]; // Negative translation (tx, ty), zoom factor = 1.5

    const [realX, realY] = applyInverseTransform(x, y, transformMatrix);

    expect(realX).toBe((50 - (-10)) / 1.5); // Expected: (60 / 1.5) = 40
    expect(realY).toBe((75 - (-20)) / 1.5); // Expected: (95 / 1.5) ≈ 63.33
  });

  it('should handle zero zoom factor gracefully', () => {
    const x = 100;
    const y = 100;
    const transformMatrix = [0, 0, 1]; // Translation (tx, ty) = (0, 0), zoom factor = 1

    const [realX, realY] = applyInverseTransform(x, y, transformMatrix);

    expect(realX).toBe(100); // No change expected
    expect(realY).toBe(100); // No change expected
  });

  it('should handle zoom factor greater than 1', () => {
    const x = 300;
    const y = 400;
    const transformMatrix = [50, 100, 5]; // Zoom factor = 5

    const [realX, realY] = applyInverseTransform(x, y, transformMatrix);

    expect(realX).toBe((300 - 50) / 5); // Expected: (250 / 5) = 50
    expect(realY).toBe((400 - 100) / 5); // Expected: (300 / 5) = 60
  });

  it('should handle zoom factor less than 1 (zoom out)', () => {
    const x = 200;
    const y = 150;
    const transformMatrix = [20, 30, 0.5]; // Zoom factor = 0.5

    const [realX, realY] = applyInverseTransform(x, y, transformMatrix);

    expect(realX).toBe((200 - 20) / 0.5); // Expected: (180 / 0.5) = 360
    expect(realY).toBe((150 - 30) / 0.5); // Expected: (120 / 0.5) = 240
  });

  it('should handle large zoom factor correctly', () => {
    const x = 500;
    const y = 500;
    const transformMatrix = [100, 200, 10]; // Zoom factor = 10

    const [realX, realY] = applyInverseTransform(x, y, transformMatrix);

    expect(realX).toBe((500 - 100) / 10); // Expected: (400 / 10) = 40
    expect(realY).toBe((500 - 200) / 10); // Expected: (300 / 10) = 30
  });

  test('screenSize function', () => {
    document.body.innerHTML = `
      <div id="globalViz">
        <svg id="d3Viz"></svg>
      </div>
    `;

    const test = document.getElementById('d3Viz');
    const testWidth = test?.clientWidth;
    const testHeight = test?.clientHeight;

    const size = screenSize(mockDivElement);
    expect(size).toStrictEqual([testWidth, testHeight]);
  });

  test('stringToRGB function', () => {
    const compartment1 = 'cytosolic';
    const compartment2 = undefined;
    const compartment3 = 'c';

    const compartmentColor1 = stringToRGB(compartment1);
    const compartmentColor2 = stringToRGB(compartment2);
    const compartmentColor3 = stringToRGB(compartment3);

    expect(compartmentColor1).toBe("#027B5F");
    expect(compartmentColor2).toBeUndefined();
    expect(compartmentColor3).toBe("#010000");
  });
});