import { expect, test, describe } from 'vitest';

import { getLinkStyle } from '../LinkStyles';

const link1 = {
  id: 'Link 4',
  label: 'Link 4',
  classes: [],
  source: {
    id: 'Acetyl',
    label: 'Acetyl',
    metadata: {},
    x: 0,
    y: 0
  },
  target: {
    id: 'Acetyl',
    label: 'Acetyl',
    metadata: {},
    x: 5000,
    y: 5000
  }
};

const link2 = {
  id: 'Link 4',
  label: 'Link 4',
  classes: ['PPI'],
  source: {
    id: 'Acetyl',
    label: 'Acetyl',
    classes: ['protein'],
    metadata: {},
    x: 0,
    y: 0
  },
  target: {
    id: 'Acetyl',
    label: 'Acetyl',
    classes: ['protein'],
    metadata: {},
    x: 5000,
    y: 5000
  }
};

const GraphStyleProperties = {
  nodeStyles: {
    'protein': {
      height: 30,
      width: 30,
      fill: 'white',
      strokeWidth: 2,
      displayLabel: true,
      rx: 15,
      ry: 15
    }
  },
  linkStyles: {
    'PPI': {
      display: true,
      stroke: 'red',
      strokeWidth: 2
    }
  }
};

describe('Test LinkStyles utils', () => {
  test('getLinkStyle function', () => {
    const PPIStyle = {
      display: true,
      stroke: 'red',
      strokeWidth: 2,
      opacity: 0.5
    }

    const defaultStyle = {
      display: true,
      stroke: '#000000',
      strokeWidth: 2,
      opacity: 0.5
    }

    // const link1Style = getLinkStyle(link1, GraphStyleProperties.linkStyles);
    // const link2Style = getLinkStyle(link2, GraphStyleProperties.linkStyles);

    // expect(link1Style).toStrictEqual(defaultStyle);
    // expect(link2Style).toStrictEqual(PPIStyle);
  });
});