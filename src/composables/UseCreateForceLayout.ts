import { forceSimulation, forceCenter, forceLink, forceManyBody, forceX, forceY, forceCollide } from 'd3-force';
import Graph from 'graphology';
import louvain from 'graphology-communities-louvain';
import { reactive } from 'vue';
import type { Node, Network, Link, ForceParams } from '@/types';

export const forceObject = reactive({
  simulation: undefined,
  isAnim: false,
  forceParams: {
    charge: {
      strength: -250,
      min: 1,
      max: 1000
    },
    collide: {
      strength: 15,
      radius: 15,
      iteration: 1
    },
    gravity: {
      strength: 6
    },
    link: {
      distance: 30,
      iteration: 1
    }
  } as ForceParams
});


/**
 * Take a network and apply a d3 force layout algorithm on WITHOUT simulation
 * @param network Network object
 * @returns {Network} Network object with d3 force layout apply on
 */
export function createStaticForceLayout(network: Network, callBack: Function = () => {}): void {
  let svgHeight = screen.height;
  let svgWidth = screen.width;

  const simulation = forceSimulation(Object.values(network.nodes))
    .force('link', forceLink()
      .id((d: any) => {
        return d.id;
      })
      .links(network.links)
    )
    .force('charge', forceManyBody())
    .force('center', forceCenter(svgWidth / 2, svgHeight / 2))
    .alphaMin(0.4)
    .stop();

  sendTick();

  function sendTick() {
    for (let i = simulation.alpha(); i > 0.90; i = simulation.alpha()) {
      simulation.tick();
    }
  }

  forceObject.simulation = simulation as any;

  callBack();
}

export function createClusterForceLayout(network: Network, callBack: Function = () => {}): void {
  let svgHeight = screen.height;
  let svgWidth = screen.width;
  const tpmNetwork = network;
  // Step 1: Convert the network to a Graphology graph for clustering
  const graph = new Graph();
  Object.keys(tpmNetwork.nodes).forEach(nodeID => graph.addNode(nodeID));
  tpmNetwork.links.forEach(link => graph.addEdge(link.source.id, link.target.id));

  // Step 2: Apply Louvain Modularity to cluster nodes
  const clusters = louvain(graph);
  Object.keys(tpmNetwork.nodes).forEach(nodeID => {
    tpmNetwork.nodes[nodeID].metadata.cluster = clusters[nodeID];
  });

  // Step 3: Aggregate nodes and links by cluster
  const clusterMap = {} as {[key: string]: Array<Node>};
  const arrayCluster = [];
  const arrayClusterLink = [];
  const clusterLink = {} as {[key: string]: Array<Link>};

  Object.keys(tpmNetwork.nodes).forEach((nodeID: string) => {
    const clusterNumber = tpmNetwork.nodes[nodeID].metadata.cluster.toString();
    if (Object.keys(clusterMap).includes(clusterNumber)) {
      clusterMap[clusterNumber].push(tpmNetwork.nodes[nodeID]);
    } else {
      clusterMap[clusterNumber] = [tpmNetwork.nodes[nodeID]];
      arrayCluster.push({id: clusterNumber});
    }
  });

  tpmNetwork.links.forEach((link: Link) => {
    const sourceCluster = clusters[link.source.id].toString();
    const targetCluster = clusters[link.target.id].toString();
    if (sourceCluster === targetCluster) {
      if (Object.keys(clusterLink).includes(sourceCluster)) {
        clusterLink[sourceCluster].push(link);
      } else {
        clusterLink[sourceCluster] = [link];
      }
    } else {
      arrayClusterLink.push({source: sourceCluster, target: targetCluster, id: sourceCluster + ' - ' + targetCluster});
    }
  });

  // Step 4: Run D3 force simulation on the aggregated graph and get clusters positions
  forceSimulation(arrayCluster)
    .force('link', forceLink(arrayClusterLink).id((d: any) => d.id).distance(50))
    .force('charge', forceManyBody().strength(-250).distanceMin(10).distanceMax(50))
    .force('center', forceCenter(svgWidth / 2, svgHeight / 2)) // Adjust based on canvas size
    .stop()
    .tick(300);

  const clusterPosition = {};
  arrayCluster.forEach((nodeCluster: Node) => {
    clusterPosition[nodeCluster.id] = {x: nodeCluster.x, y: nodeCluster.y};
  });

  Object.keys(tpmNetwork.nodes).forEach((nodeID: string) => {
    tpmNetwork.nodes[nodeID].x = clusterPosition[tpmNetwork.nodes[nodeID].metadata.cluster.toString()].x;
    tpmNetwork.nodes[nodeID].y = clusterPosition[tpmNetwork.nodes[nodeID].metadata.cluster.toString()].y;
  });



  // Step 5: Expand clusters to individual nodes
  Object.keys(clusterMap).forEach((cluster:string) => {
    forceSimulation(clusterMap[cluster])
      .force('link', forceLink(clusterLink[cluster]).id((d: any) => d.id).distance(50))
      .force('charge', forceManyBody().strength(-250).distanceMax(50))
      .force('center', forceCenter(svgWidth / 2, svgHeight / 2)) // Adjust based on canvas size
      .stop()
      .tick(30);
  });

  Object.keys(tpmNetwork.nodes).forEach((nodeID: string) => {
    network.nodes[nodeID].x = tpmNetwork.nodes[nodeID].x;
    network.nodes[nodeID].y = tpmNetwork.nodes[nodeID].y;
  });

  callBack();
}

export async function createForceWorkerLayout(network: Network): Promise<void> {
  const workerCode = `
    importScripts('https://unpkg.com/d3@5.14.2/dist/d3.min.js');

    function runLayout(graph) {
      const nodes = Object.values(graph.nodes);
      const links = graph.links;

      const simulation = d3.forceSimulation(nodes)
        .force('link', d3.forceLink(links).id(d => d.id).distance(30)) // Adjust link distance for faster convergence
        .force('charge', d3.forceManyBody().strength(-100).distanceMax(300)) // Weaker repulsion, limited distance
        .force('center', d3.forceCenter())
        .alphaDecay(0.05) // Faster cooling for quicker stabilization
        .stop();

      sendTick();

      function sendTick() {
        for (let i = simulation.alpha(); i > 0.90; i = simulation.alpha()) {
          simulation.tick();
        }
      }

    self.onmessage = event => {
      const graph = event.data;
      const result = runLayout(graph);
      self.postMessage(result); // Send final positions
    };
  `;
  return new Promise(resolve => {
    const workerBlob = new Blob([workerCode], { type: "text/javascript" });
    const workerUrl = URL.createObjectURL(workerBlob);
    const worker = new Worker(workerUrl);

    worker.onmessage = event => {
      const result = event.data;

      // Update the network nodes with final positions
      Object.keys(result).forEach((nodeID: string) => {
        network.nodes[nodeID].x = result[nodeID].x;
        network.nodes[nodeID].y = result[nodeID].y;
      });

      resolve(); // Resolve when done
      worker.terminate();
      URL.revokeObjectURL(workerUrl);
    };

    // Send network to the worker
    const sanitizedNetwork = JSON.parse(JSON.stringify(network));
    worker.postMessage(sanitizedNetwork);
  });
}


/**
 * Take a network and apply a d3 force layout algorithm on WITH simulation
 * @param network Network object
 * @returns {Network} Network object with d3 force layout apply on
 */
export function createForceLayout(network: Network, callBack: Function = () => {}): void {
  let svgHeight = screen.height;
  let svgWidth = screen.width;

  const simulation = forceSimulation(Object.values(network.nodes))
    .force('link', forceLink()
      .id((d: any) => {
        return d.id;
      })
      .links(network.links)
    )
    .force('charge', forceManyBody())
    .force('center', forceCenter(svgWidth / 2, svgHeight / 2))
    .on('tick', () => {
      if (simulation.alpha() < 0.001) {
        stopForceLayout();
      }
    });

  forceObject.simulation = simulation as any;
  forceObject.isAnim = true;

  callBack();
}

export function addNewParams(param: ForceParams): void {
  forceObject.forceParams = param;
}

export function applyParamsToForceLayout(network: Network, callBack: Function = () => {}): void {
  const simulation = forceObject.simulation;
  const params = forceObject.forceParams;

  if (simulation) {
    const manyBody = forceManyBody()
      .strength(params.charge.strength)
      .distanceMin(params.charge.min)
      .distanceMax(params.charge.max);
    
    const forceLinks = forceLink()
      .id((d: any) => {
        return d.id
      })
      .links(network.links)
      .distance(params.link.distance)
      .iterations(params.link.iteration);

    const collide = forceCollide()
      .radius(params.collide.radius)
      .strength(params.collide.strength)
      .iterations(params.collide.iteration);

    simulation
      .force('charge', manyBody)
      .force('collide', collide)
      .force('link', forceLinks)
      .force('x', forceX().strength(params.gravity.strength/1000))
      .force('y', forceY().strength(params.gravity.strength/1000))
      .on('tick', () => {
        if (simulation.alpha() < 0.001) {
          stopForceLayout();
        }
      });

    playForceLayout();
    callBack();
  }
}

export function playForceLayout() {
  forceObject.simulation.alpha(1).restart();
  forceObject.isAnim = true;
}

export function stopForceLayout() {
  forceObject.simulation.stop();
  forceObject.isAnim = false;
}