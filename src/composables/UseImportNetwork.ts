import type { Convexhulls, Network, GraphStyleProperties } from "@/types";

import { readJsonGraph } from "./ReadJsonGraph";

/**
 * Import network at JSONGraph format from an URL.
 * @param url URL to get network data
 * @param network Reference to network object
 * @param networkStyle Reference to networkStyle object
 * @param callbackFunction Function to call after network load (opt)
 */
export async function importNetworkFromURL(url: string): Promise<{network: Network, networkStyle: GraphStyleProperties, mappings: {[key: string]: any}, convexhulls: Convexhulls}> {
	const json = await getContentFromURL(url);
	const graphData = readJsonGraph(json);
	return graphData;
}

/**
 * Import network at JSONGraph format from a file.
 * @param file File to get network data
 * @param network Reference to network object
 * @param networkStyle Reference to networkStyle object
 * @param callbackFunction Function to call after network load (opt)
 */
export async function importNetworkFromFile(file: File): Promise<{network: Network, networkStyle: GraphStyleProperties, mappings: {[key: string]: any}, convexhulls: Convexhulls}> {
	const promise = new Promise((resolve) => {
		const reader = new FileReader();
		reader.onload = function () {
			const data = reader.result as string;
			const networkData = readJsonGraph(data);
			resolve(networkData);
		}	
		reader.readAsText(file);
	})
	
	const networkData = promise.then((networkData) => {
		return networkData as {network: Network, networkStyle: GraphStyleProperties, mappings: {[key: string]: any}, convexhulls: Convexhulls};
	});
	return networkData;
}



/**
 * Fetch url to return data
 * @param url URL to fetch 
 * @returns Return response
 */
async function getContentFromURL(url: string): Promise<string> {
  try {
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error('La requête a échoué avec le statut ' + response.status);
    }
    const content = await response.text();
    return content;
  } catch (error) {
    console.error('Une erreur s\'est produite lors de la récupération du contenu du fichier :', error);
    throw error;
  }
}