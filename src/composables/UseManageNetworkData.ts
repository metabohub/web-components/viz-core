import type { GraphStyleProperties } from '../types/GraphStyleProperties';
import type { Network } from '../types/Network';
import type { Node } from '../types/Node';


/**
 * Check if nodes of network have pos set to 0, 0. And returns a boolean according to a percentage threshold.
 * @param network Network object
 * @param threshold Threshold, must be in % (between 0 and 100)
 * @returns boolean according to check. 
 */
export function checkNodesPosition(network: Network, threshold: number): boolean {
  let totalNodes = Object.keys(network.nodes).length;
  const nodesThreshold = (totalNodes * threshold) / 100;

  Object.keys(network.nodes).forEach((nodeID: string) => {
    const node = network.nodes[nodeID];
    if (node.x === 0 && node.y === 0) {
      totalNodes -= 1;
    }
  });

  if (totalNodes < nodesThreshold) {
    return true;
  } else {
    return false;
  }
}

// ----------------- REMOVE FUNCTIONS ---------------------

/**
 * Remove specific node from network object
 * @param nodeId ID of node to remove
 * @param network Network object
 * @param [rIsoNode=false] Boolean to authorize or not to remove all isolated node
 */
export function removeNode(nodeId: string, network: Network, rIsoNode: boolean = false): void {
  if (network.nodes[nodeId]) {
    delete network.nodes[nodeId];

    const links = network.links.filter((link) => {
      if (link.source.id !== nodeId && link.target.id !== nodeId) {
        return link;
      }
    });

    network.links = links;
  }

  if (rIsoNode) {
    removeIsolatedNodes(network);
  }
}

/**
 * Remove a list of nodes from network object
 * @param listId Array of nodes id
 * @param network Network object
 * @param [rIsoNode=false] Boolean to authorize or not to remove all isolated node
 */
export function removeAllSelectedNodes(network: Network, rIsoNode: boolean = false): void {
  const stillDisplayNodes = [];
  Object.keys(network.nodes).forEach((nodeID: string) => {
    const node = network.nodes[nodeID];
    if (node.selected) {
      delete network.nodes[nodeID];
    } else {
      stillDisplayNodes.push(nodeID);
    }
  });

  const links = network.links.filter((link) => {
    if (stillDisplayNodes.includes(link.source.id) && stillDisplayNodes.includes(link.target.id)) {
      return link;
    }
  });

  network.links = links;

  if (rIsoNode) {
    removeIsolatedNodes(network);
  }
}

/**
 * Remove all nodes that are not connected with any other node
 * @param network Network object
 */
export function removeIsolatedNodes(network: Network): void {
  Object.keys(network.nodes).forEach((nodeID: string) => {
    const delNode = network.links.filter((link) => {
      if (link.source.id === nodeID || link.target.id === nodeID) {
        return true;
      }
    });

    if (delNode.length === 0) {
      delete network.nodes[nodeID];
    }
  });
}

/**
 * Remove all nodes according to a specific metadata attribut
 * @param network Network object that contains all nodes
 * @param attribut Metadata attribut of node. Must be a boolean
 * @param [rIsoNode=false] Boolean to authorize or not to remove all isolated node
 */
export function removeAllNodesByAttribut(network: Network, attribut: string, rIsoNode: boolean = false): void {
  Object.keys(network.nodes).forEach((nodeID: string) => {
    const node = network.nodes[nodeID] as Node;
    if (node.metadata && node.metadata[attribut]) {
      const checkAtt = node.metadata[attribut] as boolean;
      if (checkAtt) {
        removeNode(nodeID, network);
      }
    }
  });

  if (rIsoNode) {
    removeIsolatedNodes(network);
  }
}

// ----------------- DUPLICATE FUNCTIONS ---------------------

/**
 * Duplicate specific node in network object
 * @param nodeId Node id
 * @param network Network object
 * @param networkStyle Style object
 */
export function duplicateNode(nodeId: string, network: Network, networkStyle: GraphStyleProperties): Array<string> {
  if (
    network.nodes[nodeId] && (
      !(network.nodes[nodeId].metadata) ||
      (network.nodes[nodeId].metadata && !(network.nodes[nodeId].metadata.duplicate))
    )
  ) {
    const linksIndex = [] as Array<number>;
    const originalNode = network.nodes[nodeId];
    const listNewId = [] as Array<string>;

    if (networkStyle.nodeStyles) {
      if (!(Object.keys(networkStyle.nodeStyles).includes('duplicate'))) {
        networkStyle.nodeStyles['duplicate'] = {
          fill: '#FFFFFF',
          height: 10,
          width: 10,
          shape: 'circle',
        }
      }
    } else {
      networkStyle['nodeStyles'] = {
        duplicate: {
          fill: '#FFFFFF',
          height: 10,
          width: 10,
          shape: 'circle',
        }
      }
    }

    network.links.forEach((link) => {
      if (link.source.id === nodeId || link.target.id === nodeId) {
        const index = network.links.indexOf(link);
        linksIndex.push(index);
      }
    });

    for (let i = 0; i < linksIndex.length; i++) {
      const newNodeId = nodeId + '_copy-' + i;
      const newNode: Node = {
        id: newNodeId,
        classes: ['duplicate'],
        label: originalNode.label,
        x: 0,
        y: 0,
        metadata: {}
      };

      if (originalNode.metadata) {
        Object.keys(originalNode.metadata).forEach((metadata: string) => {
          newNode.metadata[metadata] = originalNode.metadata[metadata];
        });
      }

      newNode.metadata['duplicate'] = true;

      const index = linksIndex[i];

      if (network.links[index].source.id === nodeId) {
        newNode.x = originalNode.x - ((originalNode.x - network.links[index].target.x) / 2);
        newNode.y = originalNode.y - ((originalNode.y - network.links[index].target.y) / 2);
        network.links[index].source = newNode;
      }

      if (network.links[index].target.id === nodeId) {
        newNode.x = originalNode.x - ((originalNode.x - network.links[index].source.x) / 2);
        newNode.y = originalNode.y - ((originalNode.y - network.links[index].source.y) / 2);
        network.links[index].target = newNode
      }

      network.nodes[newNodeId] = newNode;
      listNewId.push(newNodeId);
    }

    delete network.nodes[nodeId];
    return listNewId;
  }
}

/**
 * Duplicate all nodes according to a specific metadata attribut
 * @param network Network object that contains all nodes
 * @param attribut Metadata attribut of node. Must be a boolean
 */
export function duplicateAllNodesByAttribut(network: Network, networkStyle: GraphStyleProperties, attribut: string): void {
  Object.keys(network.nodes).forEach((nodeID: string) => {
    const node = network.nodes[nodeID] as Node;
    if (node.metadata && node.metadata[attribut]) {
      const checkAtt = node.metadata[attribut] as boolean;
      if (checkAtt) {
        duplicateNode(nodeID, network, networkStyle);
      }
    }
  });
}

/**
 * Switch between line path and curve path
 * @param networkStyle Network style object
 */
export function switchLineStyle(networkStyle: GraphStyleProperties): void {
  if (networkStyle.curveLine) {
    networkStyle.curveLine = !networkStyle.curveLine;
  } else {
    networkStyle.curveLine = true;
  }
}