/**
 * Compute path with double arrow head between two nodes
 * @param sourceX Position x of start node
 * @param sourceY Position y of start node
 * @param targetX Position x of target node
 * @param targetY Position y of target node
 * @param sourceWidth Source node width
 * @param sourceHeight Source node height
 * @param targetWidth Target node width
 * @param targetHeight Target node height
 * @returns {string} Path for the link
 */
export function reversibleDirectedLinePath(
	sourceX: number,
	sourceY: number,
	targetX: number,
	targetY: number,
	sourceWidth: number,
	sourceHeight: number,
	targetWidth: number,
	targetHeight: number
): string {
	const xSource = sourceX + (sourceWidth / 2);
	const ySource = sourceY + (sourceHeight / 2);

	let xTarget = targetX + (targetWidth / 2);
	let yTarget = targetY + (targetHeight / 2);

	const d = Math.sqrt(
		Math.pow(xTarget - xSource, 2) + Math.pow(yTarget - ySource, 2)
	);
	const dX = xTarget - xSource;
	const dY = yTarget - ySource;

	const rTW = (Math.abs(d) * targetWidth / 2) / Math.abs(dX);
	const rTH = (Math.abs(d) * targetHeight / 2) / Math.abs(dY);
	const largeurNoeudT = rTW < rTH ? rTW : rTH;

	xTarget = xSource + dX * ((d - largeurNoeudT) / d);
	yTarget = ySource + dY * ((d - largeurNoeudT) / d);

	var heightArrow = 5;
	var xBaseArrowT = xSource + dX * ((d - largeurNoeudT - heightArrow) / d);
	var yBaseArrowT = ySource + dY * ((d - largeurNoeudT - heightArrow) / d);

	var xBaseArrowRev = xSource + dX * ((d - largeurNoeudT - heightArrow - heightArrow) / d);
	var yBaseArrowRev = ySource + dY * ((d - largeurNoeudT - heightArrow - heightArrow) / d);

	var xWBaseArrowT1 = xBaseArrowT + dY * (3 / d);
	var yWBaseArrowT1 = yBaseArrowT - dX * (3 / d);
	var xWBaseArrowT2 = xBaseArrowT - dY * (3 / d);
	var yWBaseArrowT2 = yBaseArrowT + dX * (3 / d);

	return "M" + xSource + "," + ySource +
		"L" + xBaseArrowRev + "," + yBaseArrowRev +
		"L" + xWBaseArrowT1 + "," + yWBaseArrowT1 +
		"L" + xWBaseArrowT2 + "," + yWBaseArrowT2 +
		"L" + xTarget + "," + yTarget +
		"L" + xWBaseArrowT1 + "," + yWBaseArrowT1 +
		"L" + xWBaseArrowT2 + "," + yWBaseArrowT2 +
		"L" + xBaseArrowRev + "," + yBaseArrowRev;
}



/**
 * Compute path with arrow head between two nodes
 * @param sourceX Position x of start node
 * @param sourceY Position y of start node
 * @param targetX Position x of target node
 * @param targetY Position y of target node
 * @param sourceWidth Source node width
 * @param sourceHeight Source node height
 * @param targetWidth Target node width
 * @param targetHeight Target node height
 * @returns {string} Path for the link
 */
export function directedLinePath(
	sourceX: number,
	sourceY: number,
	targetX: number,
	targetY: number,
	sourceWidth: number,
	sourceHeight: number,
	targetWidth: number,
	targetHeight: number
): string {

	const xSource = sourceX + sourceWidth / 2;
	const ySource = sourceY + sourceHeight / 2;

	// Compute the center of the target node
	let xTarget = targetX + targetWidth / 2;
	let yTarget = targetY + targetHeight / 2;

	// Compute distance and direction
	const d = Math.sqrt(Math.pow(xTarget - xSource, 2) + Math.pow(yTarget - ySource, 2));
	const dX = xTarget - xSource;
	const dY = yTarget - ySource;

	// Adjust for node boundaries
	const targetBoundary = Math.min(targetWidth, targetHeight) / 2;
	xTarget = xSource + (dX * (d - targetBoundary)) / d;
	yTarget = ySource + (dY * (d - targetBoundary)) / d;

	// Arrowhead size
	const arrowHeight = 10; // Height of the arrowhead
	const arrowWidth = 6; // Half-width of the diamond

	// Arrowhead points
	const tipX = xTarget;
	const tipY = yTarget;

	const baseX = xSource + (dX * (d - targetBoundary - arrowHeight)) / d;
	const baseY = ySource + (dY * (d - targetBoundary - arrowHeight)) / d;

	const leftX = baseX + (dY * arrowWidth) / d;
	const leftY = baseY - (dX * arrowWidth) / d;

	const rightX = baseX - (dY * arrowWidth) / d;
	const rightY = baseY + (dX * arrowWidth) / d;

	// Rounded corners: Use quadratic curves
	return (
		`M${xSource},${ySource} ` + // Start at the source
		`L${baseX},${baseY} ` + // Line to arrow base
		`Q${leftX},${leftY} ${tipX},${tipY} ` + // Curve to the tip (left side)
		`Q${rightX},${rightY} ${baseX},${baseY} ` + // Curve back to base (right side)
		`Z` // Close the arrowhead path
	);
}

/**
 * Compute line path between two nodes
 * @param sourceX Position x of start node
 * @param sourceY Position y of start node
 * @param targetX Position x of target node
 * @param targetY Position y of target node
 * @param sourceWidth Source node width
 * @param sourceHeight Source node height
 * @param targetWidth Target node width
 * @param targetHeight Target node height
 * @returns {string} Path for the link
 */
export function linePath(
	sourceX: number,
	sourceY: number,
	targetX: number,
	targetY: number,
	sourceWidth: number,
	sourceHeight: number,
	targetWidth: number,
	targetHeight: number
): string {

	const xSource = sourceX + (sourceWidth / 2);
	const ySource = sourceY + (sourceHeight / 2);

	let xTarget = targetX + (targetWidth / 2);
	let yTarget = targetY + (targetHeight / 2);

	const dX = xTarget - xSource;
	const dY = yTarget - ySource;

	xTarget = xSource + dX;
	yTarget = ySource + dY;


	return (
		"M" + xSource + "," + ySource +
		"L" + xTarget + "," + yTarget
	);
}


/**
 * Return curve path string for links
 * @param sourceX X position of source node
 * @param sourceY Y position of source node
 * @param targetX X position of target node
 * @param targetY Y position of target node
 * @param sourceWidth Width of source node
 * @param sourceHeight Height of source node
 * @param targetWidth Width of target node
 * @param targetHeight Height of target node
 * @returns {string} String of path for curve line
 */
export function curvedLinePath(
	sourceX: number,
	sourceY: number,
	targetX: number,
	targetY: number,
	sourceWidth: number,
	sourceHeight: number,
	targetWidth: number,
	targetHeight: number,
	exiting: boolean = false,
	shift: number = 0
): string {


	let xSource = sourceX + (sourceWidth / 2);
	let ySource = sourceY + (sourceHeight / 2);

	let xTarget = targetX + (targetWidth / 2);
	let yTarget = targetY + (targetHeight / 2);

	const dX = xTarget - xSource;
	const dY = yTarget - ySource;

	xTarget = xSource + dX;
	yTarget = ySource + dY;

	let enteringX = xSource;
	let enteringY = ySource;

	if (Math.abs(xSource - xTarget) > Math.abs(ySource - yTarget)) {
		if (xSource < xTarget) {
			enteringX -= targetWidth / 2 + 10;
		}
		else {
			enteringX += targetWidth / 2 + 10;
		}
	}
	else {
		if (ySource < yTarget) {
			enteringY -= targetHeight / 2 + 10;
		}
		else {
			enteringY += targetHeight / 2 + 10;
		}
	}
	var exitingX = xSource - (enteringX - xSource);
	var exitingY = ySource - (enteringY - ySource);

	let path = ''

	// if (shift !== undefined) {
	// 	xSource += shift;
	// 	ySource += shift;
	// 	exitingX += shift;
	// 	exitingY += shift;
	// 	xTarget += shift;
	// 	yTarget += shift;
	// }

	if (exitingY == ySource) {
		path = computePathHorizontal(xSource, ySource + shift, exitingX, exitingY + shift, xTarget + shift, yTarget, targetHeight, targetWidth);
		if (exiting) {
			path = computePathHorizontalEnd(xSource, ySource + shift, exitingX, exitingY + shift, xTarget, yTarget + shift, targetHeight, targetWidth);
		}
	}
	else {
		path = computePathVertical(xSource + shift, ySource, exitingX + shift, exitingY, xTarget + shift, yTarget, targetHeight, targetWidth);
		if (exiting) {
			path = computePathVerticalEnd(xSource + shift, ySource, exitingX + shift, exitingY, xTarget + shift, yTarget, targetHeight, targetWidth);
		}
	}

	return path;
}


/**
 * Compute curve path when source and target nodes are horizontally aligned
 * @param sourceX X position of source node
 * @param sourceY Y position of source node
 * @param firstPointX X position for curve computation
 * @param firstPointY Y position for curve computation
 * @param targetX X position of target node
 * @param targetY Y position of target node
 * @param targetHeight Height of target node
 * @param targetWidth Width of target node
 * @returns {string} String of curve path
 */
function computePathHorizontal(
	sourceX: number,
	sourceY: number,
	firstPointX: number,
	firstPointY: number,
	targetX: number,
	targetY: number,
	targetHeight: number,
	targetWidth: number
) {
	// Compute the coordinates of the control point used for drawing the path
	var controlX = targetX;
	var controlY = sourceY;
	if (firstPointX < sourceX && controlX > firstPointX) {
		controlX = firstPointX - 15;
	}
	else if (firstPointX > sourceX && controlX < firstPointX) {
		controlX = firstPointX + 15;
	}
	// Compute the path of the link for 3 different cases
	var path;
	var lastPointX = targetX;
	var lastPointY = targetY;
	var beforeLastPointX = lastPointX;
	var beforeLastPointY = lastPointY;
	if (controlX == targetX) {
		// 1st case: The end node is on the correct side of the starting node, and is close to the axe of the the reaction
		if (Math.abs(targetY - sourceY) < 15) {
			if (firstPointX < sourceX) {
				lastPointX += targetWidth / 2;
				beforeLastPointX = lastPointX + 5;
			}
			else {
				lastPointX -= targetWidth / 2;
				beforeLastPointX = lastPointX - 5;
			}
			var middlePointX = (firstPointX + lastPointX) / 2;
			var middlePointY = (firstPointY + lastPointY) / 2;
			var firstSidePointX = middlePointX;
			var firstSidePointY = middlePointY;
			var secondSidePointX = middlePointX;
			var secondSidePointY = middlePointY;
			if (firstPointX == sourceX) {
				firstSidePointX = firstPointX;
				secondSidePointX = lastPointX;
			}
			else {
				firstSidePointY = firstPointY;
				secondSidePointY = lastPointY;
			}
			path = "M" + sourceX + "," + sourceY +
				"L" + firstPointX + "," + firstPointY +
				"Q" + firstSidePointX + "," + firstSidePointY + "," + middlePointX + "," + middlePointY +
				"Q" + secondSidePointX + "," + secondSidePointY + "," + beforeLastPointX + "," + beforeLastPointY +
				"L" + lastPointX + "," + lastPointY;
		}
		// 2nd case: The end node is on the correct side of the starting node, and is not close to the axe of the the reaction
		else {
			if (targetY < sourceY) {
				lastPointY += targetHeight / 2;
				beforeLastPointY = lastPointY + 5;
			}
			else {
				lastPointY -= targetHeight / 2;
				beforeLastPointY = lastPointY - 5;
			}
			path = "M" + sourceX + "," + sourceY +
				"L" + firstPointX + "," + firstPointY +
				"Q" + controlX + "," + controlY + "," + beforeLastPointX + "," + beforeLastPointY +
				"L" + lastPointX + "," + lastPointY;
		}
	}
	// 3rd case: The end node is not on the correct side of the reaction
	else {
		if (firstPointX < sourceX) {
			lastPointX = targetX - (targetWidth / 2);
			beforeLastPointX = lastPointX - 5;
		}
		else {
			lastPointX = targetX + (targetWidth / 2);
			beforeLastPointX = lastPointX + 5;
		}
		var control2X = controlX;
		var control2Y = targetY;
		path = "M" + sourceX + "," + sourceY +
			"L" + firstPointX + "," + firstPointY +
			"C" + controlX + "," + controlY + "," + control2X + "," + control2Y + "," + beforeLastPointX + "," + beforeLastPointY +
			"L" + lastPointX + "," + lastPointY;
	}
	return path;
}

/**
 * Compute curve path when source and target nodes are vertically aligned
 * @param sourceX X position of source node
 * @param sourceY Y position of source node
 * @param firstPointX X position for curve computation
 * @param firstPointY Y position for curve computation
 * @param targetX X position of target node
 * @param targetY Y position of target node
 * @param targetHeight Height of target node
 * @param targetWidth Width of target node
 * @returns {string} String of curve path
 */
function computePathVertical(
	sourceX: number,
	sourceY: number,
	firstPointX: number,
	firstPointY: number,
	targetX: number,
	targetY: number,
	targetWidth: number,
	targetHeight: number
) {
	// Compute the coordinates of the control point used for drawing the path
	var controlX = sourceX;
	var controlY = targetY;
	if (firstPointY < sourceY && controlY > firstPointY) {
		controlY = firstPointY - 15;
	}
	else if (firstPointY > sourceY && controlY < firstPointY) {
		controlY = firstPointY + 15;
	}
	// Compute the path of the link for 3 different cases
	var path;
	var lastPointX = targetX;
	var lastPointY = targetY;
	var beforeLastPointX = lastPointX;
	var beforeLastPointY = lastPointY;
	if (controlY == targetY) {
		// 1st case: The end node is on the correct side of the starting node, and is close to the axe of the the reaction
		if (Math.abs(targetX - sourceX) < 15) {
			if (firstPointY < sourceY) {
				lastPointY += targetHeight / 2;
				beforeLastPointY = lastPointY + 5;
			}
			else {
				lastPointY -= targetHeight / 2;
				beforeLastPointY = lastPointY - 5;
			}
			var middlePointX = (firstPointX + lastPointX) / 2;
			var middlePointY = (firstPointY + lastPointY) / 2;
			var firstSidePointX = middlePointX;
			var firstSidePointY = middlePointY;
			var secondSidePointX = middlePointX;
			var secondSidePointY = middlePointY;
			if (firstPointX == sourceX) {
				firstSidePointX = firstPointX;
				secondSidePointX = lastPointX;
			}
			else {
				firstSidePointY = firstPointY;
				secondSidePointY = lastPointY;
			}
			path = "M" + sourceX + "," + sourceY +
				"L" + firstPointX + "," + firstPointY +
				"Q" + firstSidePointX + "," + firstSidePointY + "," + middlePointX + "," + middlePointY +
				"Q" + secondSidePointX + "," + secondSidePointY + "," + beforeLastPointX + "," + beforeLastPointY +
				"L" + lastPointX + "," + lastPointY;
		}
		// 2nd case: The end node is on the correct side of the starting node, and is not close to the axe of the the reaction
		else {
			if (targetX < sourceX) {
				lastPointX += targetWidth / 2;
				beforeLastPointX = lastPointX + 5;
			}
			else {
				lastPointX -= targetWidth / 2;
				beforeLastPointX = lastPointX - 5;
			}
			path = "M" + sourceX + "," + sourceY +
				"L" + firstPointX + "," + firstPointY +
				"Q" + controlX + "," + controlY + "," + beforeLastPointX + "," + beforeLastPointY +
				"L" + lastPointX + "," + lastPointY;
		}
	}
	// 3rd case: The end node is not on the correct side of the reaction
	else {
		if (firstPointY < sourceY) {
			lastPointY = targetY - (targetWidth / 2);
			beforeLastPointY = lastPointY - 5;
		}
		else {
			lastPointY = targetY + (targetWidth / 2);
			beforeLastPointY = lastPointY + 5;
		}
		var control2X = targetX;
		var control2Y = controlY;
		path = "M" + sourceX + "," + sourceY +
			"L" + firstPointX + "," + firstPointY +
			"C" + controlX + "," + controlY + "," + control2X + "," + control2Y + "," + beforeLastPointX + "," + beforeLastPointY +
			"L" + lastPointX + "," + lastPointY;
	}
	return path;
}

/**
 * Compute curve path when source and target nodes are horizontally aligned
 * @param sourceX X position of source node
 * @param sourceY Y position of source node
 * @param firstPointX X position for curve computation
 * @param firstPointY Y position for curve computation
 * @param targetX X position of target node
 * @param targetY Y position of target node
 * @param targetHeight Height of target node
 * @param targetWidth Width of target node
 * @returns {string} String of curve path
 */
function computePathHorizontalEnd(
	sourceX: number,
	sourceY: number,
	firstPointX: number,
	firstPointY: number,
	targetX: number,
	targetY: number,
	targetHeight: number,
	targetWidth: number
) {
	// Compute the coordinates of the control point used for drawing the path
	var controlX = targetX;
	var controlY = sourceY;
	if (firstPointX < sourceX && controlX > firstPointX) {
		controlX = firstPointX - 15;
	}
	else if (firstPointX > sourceX && controlX < firstPointX) {
		controlX = firstPointX + 15;
	}
	// Compute the path of the link for 3 different cases
	var path;
	var lastPointX = targetX;
	var lastPointY = targetY;
	var beforeLastPointX = lastPointX;
	var beforeLastPointY = lastPointY;
	if (controlX == targetX) {
		// 1st case: The end node is on the correct side of the starting node, and is close to the axe of the the reaction
		if (Math.abs(targetY - sourceY) < 15) {
			if (firstPointX < sourceX) {
				lastPointX += targetWidth / 2;
				beforeLastPointX = lastPointX + 5;
			}
			else {
				lastPointX -= targetWidth / 2;
				beforeLastPointX = lastPointX - 5;
			}
			var middlePointX = (firstPointX + lastPointX) / 2;
			var middlePointY = (firstPointY + lastPointY) / 2;
			var firstSidePointX = middlePointX;
			var firstSidePointY = middlePointY;
			var secondSidePointX = middlePointX;
			var secondSidePointY = middlePointY;
			if (firstPointX == sourceX) {
				firstSidePointX = firstPointX;
				secondSidePointX = lastPointX;
			}
			else {
				firstSidePointY = firstPointY;
				secondSidePointY = lastPointY;
			}

			const y1 = beforeLastPointY + 2;
			const y2 = beforeLastPointY - 2;
			const yMo = (lastPointY + beforeLastPointY) / 2

			path = "M" + sourceX + "," + sourceY +
				"L" + firstPointX + "," + firstPointY +
				"Q" + firstSidePointX + "," + firstSidePointY + "," + middlePointX + "," + middlePointY +
				"Q" + secondSidePointX + "," + secondSidePointY + "," + beforeLastPointX + "," + beforeLastPointY +
				"L" + beforeLastPointX + "," + y2 +
				"L" + lastPointX + "," + yMo +
				"L" + beforeLastPointX + "," + y1 +
				"L" + beforeLastPointX + "," + beforeLastPointY;
		}
		// 2nd case: The end node is on the correct side of the starting node, and is not close to the axe of the the reaction
		else {
			if (targetY < sourceY) {
				lastPointY += targetHeight / 2;
				beforeLastPointY = lastPointY + 5;
			}
			else {
				lastPointY -= targetHeight / 2;
				beforeLastPointY = lastPointY - 5;
			}

			const x1 = beforeLastPointX - 2;
			const x2 = beforeLastPointX + 2;
			const xMo = (beforeLastPointX + lastPointX) / 2;

			path = "M" + sourceX + "," + sourceY +
				"L" + firstPointX + "," + firstPointY +
				"Q" + controlX + "," + controlY + "," + beforeLastPointX + "," + beforeLastPointY +
				"L" + x1 + "," + beforeLastPointY +
				"L" + xMo + "," + lastPointY +
				"L" + x2 + "," + beforeLastPointY +
				"L" + beforeLastPointX + "," + beforeLastPointY;
		}
	}
	// 3rd case: The end node is not on the correct side of the reaction
	else {
		if (firstPointX < sourceX) {
			lastPointX = targetX - (targetWidth / 2);
			beforeLastPointX = lastPointX - 5;
		}
		else {
			lastPointX = targetX + (targetWidth / 2);
			beforeLastPointX = lastPointX + 5;
		}

		const control2X = controlX;
		const control2Y = targetY;
		const y1 = beforeLastPointY + 2;
		const y2 = beforeLastPointY - 2;
		const yMo = (lastPointY + beforeLastPointY) / 2

		path = "M" + sourceX + "," + sourceY +
			"L" + firstPointX + "," + firstPointY +
			"C" + controlX + "," + controlY + "," + control2X + "," + control2Y + "," + beforeLastPointX + "," + beforeLastPointY +
			"L" + beforeLastPointX + "," + y2 +
			"L" + lastPointX + "," + yMo +
			"L" + beforeLastPointX + "," + y1 +
			"L" + beforeLastPointX + "," + beforeLastPointY;
	}
	return path;
}

/**
 * Compute curve path when source and target nodes are vertically aligned with arrowhead
 * @param sourceX X position of source node
 * @param sourceY Y position of source node
 * @param firstPointX X position for curve computation
 * @param firstPointY Y position for curve computation
 * @param targetX X position of target node
 * @param targetY Y position of target node
 * @param targetHeight Height of target node
 * @param targetWidth Width of target node
 * @returns {string} String of curve path
 */
function computePathVerticalEnd(
	sourceX: number,
	sourceY: number,
	firstPointX: number,
	firstPointY: number,
	targetX: number,
	targetY: number,
	targetWidth: number,
	targetHeight: number
) {
	// Compute the coordinates of the control point used for drawing the path
	var controlX = sourceX;
	var controlY = targetY;
	if (firstPointY < sourceY && controlY > firstPointY) {
		controlY = firstPointY - 15;
	}
	else if (firstPointY > sourceY && controlY < firstPointY) {
		controlY = firstPointY + 15;
	}
	// Compute the path of the link for 3 different cases
	var path;
	var lastPointX = targetX;
	var lastPointY = targetY;
	var beforeLastPointX = lastPointX;
	var beforeLastPointY = lastPointY;
	if (controlY == targetY) {
		// 1st case: The end node is on the correct side of the starting node, and is close to the axe of the the reaction
		if (Math.abs(targetX - sourceX) < 15) {
			if (firstPointY < sourceY) {
				lastPointY += targetHeight / 2;
				beforeLastPointY = lastPointY + 5;
			}
			else {
				lastPointY -= targetHeight / 2;
				beforeLastPointY = lastPointY - 5;
			}
			var middlePointX = (firstPointX + lastPointX) / 2;
			var middlePointY = (firstPointY + lastPointY) / 2;
			var firstSidePointX = middlePointX;
			var firstSidePointY = middlePointY;
			var secondSidePointX = middlePointX;
			var secondSidePointY = middlePointY;
			if (firstPointX == sourceX) {
				firstSidePointX = firstPointX;
				secondSidePointX = lastPointX;
			}
			else {
				firstSidePointY = firstPointY;
				secondSidePointY = lastPointY;
			}

			var x1 = beforeLastPointX - 2;
			var x2 = beforeLastPointX + 2;
			var xMo = (beforeLastPointX + lastPointX) / 2;

			path = "M" + sourceX + "," + sourceY +
				"L" + firstPointX + "," + firstPointY +
				"Q" + firstSidePointX + "," + firstSidePointY + "," + middlePointX + "," + middlePointY +
				"Q" + secondSidePointX + "," + secondSidePointY + "," + beforeLastPointX + "," + beforeLastPointY +
				"L" + x1 + "," + beforeLastPointY +
				"L" + xMo + "," + lastPointY +
				"L" + x2 + "," + beforeLastPointY +
				"L" + beforeLastPointX + "," + beforeLastPointY;
		}
		// 2nd case: The end node is on the correct side of the starting node, and is not close to the axe of the the reaction
		else {
			if (targetX < sourceX) {
				lastPointX += targetWidth / 2;
				beforeLastPointX = lastPointX + 5;
			}
			else {
				lastPointX -= targetWidth / 2;
				beforeLastPointX = lastPointX - 5;
			}

			var y1 = beforeLastPointY + 2;
			var y2 = beforeLastPointY - 2;
			var yMo = (lastPointY + beforeLastPointY) / 2

			path = "M" + sourceX + "," + sourceY +
				"L" + firstPointX + "," + firstPointY +
				"Q" + controlX + "," + controlY + "," + beforeLastPointX + "," + beforeLastPointY +
				"L" + beforeLastPointX + "," + y2 +
				"L" + lastPointX + "," + yMo +
				"L" + beforeLastPointX + "," + y1 +
				"L" + beforeLastPointX + "," + beforeLastPointY;
		}
	}
	// 3rd case: The end node is not on the correct side of the reaction
	else {
		if (firstPointY < sourceY) {
			lastPointY = targetY - (targetWidth / 2);
			beforeLastPointY = lastPointY - 5;
		}
		else {
			lastPointY = targetY + (targetWidth / 2);
			beforeLastPointY = lastPointY + 5;
		}

		var control2X = targetX;
		var control2Y = controlY;
		var x1 = beforeLastPointX - 2;
		var x2 = beforeLastPointX + 2;
		var xMo = (beforeLastPointX + lastPointX) / 2;

		path = "M" + sourceX + "," + sourceY +
			"L" + firstPointX + "," + firstPointY +
			"C" + controlX + "," + controlY + "," + control2X + "," + control2Y + "," + beforeLastPointX + "," + beforeLastPointY +
			"L" + x1 + "," + beforeLastPointY +
			"L" + xMo + "," + lastPointY +
			"L" + x2 + "," + beforeLastPointY +
			"L" + beforeLastPointX + "," + beforeLastPointY;
	}
	return path;
}