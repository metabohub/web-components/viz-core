import type { Node } from "../types/Node";
import { ComputedRef, Ref, computed, onMounted, ref, watch, getCurrentInstance } from "vue";
import { drag } from 'd3-drag';
import { select } from 'd3-selection';
import type { Selection, BaseType } from 'd3-selection';
import type { DragBehavior } from 'd3-drag';

/**
 * Get current node instance to assign d3 drag and drop methods.
 * @param props properties of a node
 * @returns {ComputedRef<string>, Ref<SVGElement | undefined>} d3.drag event for a specific node and cursor style
 */
export function useDragAndDrop(props: { node: Node }): {cursor: ComputedRef<string>, nodeElement: Ref<SVGElement | undefined>} {

	const instance = getCurrentInstance();
	if (!instance) {
		throw new Error('useDragAndDrop must be called within a setup function');
	}

	// refs
	const nodeElement = ref<SVGElement>();
	const dragOffsetX = ref(0);
	const dragOffsetY = ref(0);
	const initialX = ref(0);
	const initialY = ref(0);
	const x = ref<number>(0);
	const y = ref<number>(0);

	// Lifecycle hooks 
	onMounted(() => {

		x.value = props.node.x;
		y.value = props.node.y;

		const dragBehavior: DragBehavior<SVGElement, unknown, unknown> = drag<SVGElement, unknown>()
			.on('drag', onDrag)
			.on('start', onDragStart)
			.on('end', onDragEnd)

		if (nodeElement.value) {
			const selection: Selection<SVGElement, unknown, BaseType, unknown> = select(nodeElement.value);
			dragBehavior(selection);
		}

	});

	// computed

	// To change the cursor when hovering a node
	const cursor = computed(() => {
		return `cursor: ${dragOffsetX.value ? 'grabbing' : 'grab'}`
	});

	// watchers

	watch(() => props.node.x, (newX) => {
		x.value = newX;
	});

	watch(() => props.node.y, (newY) => {
		y.value = newY;
	});

	// Methods

	const onDragStart = (event: MouseEvent) => {
		dragOffsetX.value = x.value - event.x;
		dragOffsetY.value = y.value - event.y;

		initialX.value = x.value;
		initialY.value = y.value;

		instance.emit('dragstart');
	}

	const onDragEnd = () => {
		if (initialX.value === x.value && initialY.value === y.value) {
			instance.emit('leftClickNode');
		} else {
			instance.emit('dragend');
		}
		dragOffsetX.value = dragOffsetY.value = 0;
	}

	const onDrag = (event: MouseEvent) => {
		const newX = event.x + dragOffsetX.value;
		const newY = event.y + dragOffsetY.value;
		instance.emit('drag', props.node.id, newX, newY);
	}

	return {
		cursor,
		nodeElement
	};

}
