import type { Network } from "../types/Network";
import type { Node } from "../types/Node";
import type { NodeStyle } from "../types/NodeStyle";
import { screenSize, applyInverseTransform } from "../utils/GraphUtils";
import { getNodeStyle } from "../utils/NodeStyles";
import { brush } from "d3-brush";
import { select, selectAll } from 'd3-selection';
import type { BrushBehavior } from "d3-brush";
import { reactive } from "vue";

const brushProperties = reactive({
  nodes: {} as {[key: string]: Node}
})

/**
 * Change a boolean to switch between two modes. For example between selection and zoom for graph panel
 * @param mode Current state
 * @returns The opposite of current state
 */
export function switchGraphMode(mode: boolean): boolean {
  return !(mode);
}

/**
 * Select or unselect a node
 * @param node Node object clicked
 */
export function nodeSelection(node: Node): void {
  if (node.selected) {
    node.selected = false;
  } else {
    node.selected = true;
  }
}

/**
 * Define brush tag and functions to select multiple nodes at ctrl + left click
 * @param network Network object
 * @param styles Network style object
 */
export function defineBrush(globalViz: HTMLElement, brushG: SVGGElement, network: Network, styles: {[key: string]: NodeStyle}, instance): void {
  const svgSize = screenSize(globalViz);
  const svgWidth = svgSize[0] as number;
  const svgHeight = svgSize[1] as number;

  brushProperties.nodes = network.nodes;

  function endBrush(e: any) {
    if (!e.selection){
      return; // Prevent infinity loop
    } 

    let extent = e.selection;
    
    if (extent) {
      const [[x0, y0], [x1, y1]] = extent;
      // Apply inverse transform to adjust for zoom and pan
      const inverseMatrix = [instance.getPan()['x'], instance.getPan()['y'], instance.getSizes().realZoom];
      const [realX0, realY0] = applyInverseTransform(x0, y0, inverseMatrix);
      const [realX1, realY1] = applyInverseTransform(x1, y1, inverseMatrix);
      // Handle selection logic here

      if (extent[1][0] - extent[0][0] > 20 || extent[1][1] - extent[0][1] > 20) {
        Object.keys(brushProperties.nodes).forEach((nodeID: string) => {
          const node = brushProperties.nodes[nodeID];
          const nodeStyle = getNodeStyle(node, styles);
          
          const xCenter = node.x + (nodeStyle.width as number / 2);
          const yCenter = node.y + (nodeStyle.height as number / 2);
          
          if ((realX0 <= xCenter && xCenter < realX1)
            && (realY0 <= yCenter && yCenter < realY1)){
              if (brushProperties.nodes[nodeID]) {
                if (brushProperties.nodes[nodeID].selected) {
                  brushProperties.nodes[nodeID].selected = true;
                } else {
                  brushProperties.nodes[nodeID].selected = true;
                }
              } else {
                brushProperties.nodes[nodeID].selected = true;
              }
          }
        });
      }
    }
    
    select(this).call(brushEvent.move as BrushBehavior<unknown>, null); // Remove brush layer but can create infinity loop -> need prevent at the beginning of the function
  }

  const brushEvent = brush()
    .filter((event: MouseEvent) => {
      return event.ctrlKey;
    })
    .on('end', endBrush)
    .extent([[0, 0], [svgWidth, svgHeight]]);

  select<SVGElement, unknown>(brushG)
      .call(brushEvent)
    .selectChildren()
      .style('cursor', 'default');

  select('body')
    .on('keydown', function(event: KeyboardEvent) {
      if (event.ctrlKey)
        switchCursor('crosshair', brushG);
    })
    .on('keyup', function(event: KeyboardEvent) {
      if (event.key === 'Control')
        switchCursor('default', brushG);
    });
}

/**
 * Update network for brush functions
 */
export function updateNetworkForBrush(network: Network): void {
  brushProperties.nodes = network.nodes;
}

/**
 * Remove brush tag and functions
 */
export function stopBrush(): void {
  selectAll('#brush').remove();
  select('body').dispatch('keydown');
  select('body').dispatch('keyup');
}

/**
 * Change cursor style for brush. Allow to switch between default (panzoom) and crosshair (brush)
 * @param style Cursor style to apply
 */
export function switchCursor(style: string, brushG: SVGGElement): void {
  select(brushG)
    .selectChildren()
      .style('cursor', style);
}

/**
 * Align vertically selected nodes (in list)
 * @param network Network object
 * @param nodes Nodes list
 */
export function verticalNodesAlign(network: Network, styles: {[key: string]: NodeStyle}): void {
  const nodes = [] as Array<Node>;
	Object.keys(network.nodes).forEach((nodeID: string) => {
		if (network.nodes[nodeID].selected) {
			nodes.push(network.nodes[nodeID]);
		}
	});

  if (nodes.length > 0) {
    const refNodeStyle = getNodeStyle(nodes[0], styles);
    const xRef = nodes[0].x + (refNodeStyle.width! / 2);
    nodes.shift();

    nodes.forEach((node: Node) => {
      const nodeID = node.id as string;
      const nodeStyle = getNodeStyle(node, styles);
      network.nodes[nodeID].x = xRef - (nodeStyle.width! / 2);
    });
  }
}

/**
 * Align horizontally selected nodes (in list)
 * @param network Network object
 * @param nodes Nodes list
 */
export function horizontalNodesAlign(network: Network, styles: {[key: string]: NodeStyle}): void {
  const nodes = [] as Array<Node>;
	Object.keys(network.nodes).forEach((nodeID: string) => {
		if (network.nodes[nodeID].selected) {
			nodes.push(network.nodes[nodeID]);
		}
	});

  if (nodes.length > 0) {
    const refNodeStyle = getNodeStyle(nodes[0], styles);
    const yRef = nodes[0].y + (refNodeStyle.height! / 2);
    nodes.shift();

    nodes.forEach((node: Node) => {
      const nodeID = node.id as string;
      const nodeStyle = getNodeStyle(node, styles);
      network.nodes[nodeID].y = yRef - (nodeStyle.height! / 2);
    });
  }
}

/**
 * Unselect all selected nodes
 * @param network Network object
 */
export function unselectAll(network: Network): void {
  Object.keys(network.nodes).forEach((nodeID: string) => {
    if (network.nodes[nodeID].selected) {
      network.nodes[nodeID].selected = false;
    }
  });
}

