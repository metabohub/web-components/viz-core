import svgPanZoom from 'svg-pan-zoom';

/**
 * Init svgPanZoom function over network component svg element
 * @param networkElement SVGElement of the network component
 * @returns {SvgPanZoom.Instance} Return svgPanZoom instance to use methods on network svg
 */
export function initZoom(networkElement: SVGElement, graphG: SVGGElement): SvgPanZoom.Instance {
  return svgPanZoom(networkElement, {
    viewportSelector: graphG,
		dblClickZoomEnabled: false,
		zoomScaleSensitivity: 0.8,
		minZoom: 0,
		maxZoom: 1000,
    eventsListenerElement: networkElement,
    customEventsHandler: {
      init: function (options) {
        this.listeners = {
          mousedown: function (event: MouseEvent) {
            if (!(event.ctrlKey)) {
              options.instance.enablePan();
            } else {
              options.instance.disablePan();
            }
          }
        };

        for (let eventName in this.listeners) {
          options.svgElement.addEventListener(eventName, this.listeners[eventName]);
        }
      },
      destroy: function (options) {
        for (let eventName in this.listeners) {
          options.svgElement.removeEventListener(eventName, this.listeners[eventName]);
        }
      },
      haltEventListeners: []
    }
	});
}

/**
 * Return null to remove SvgPanZoom.Instance, to stop zoom event
 * @returns null
 */
export function stopZoom(instance: SvgPanZoom.Instance): void {
  instance.destroy();
}

export function rescale(svgPanZoom: SvgPanZoom.Instance): void {
  svgPanZoom.updateBBox();
  svgPanZoom.resize();
  svgPanZoom.fit();
  svgPanZoom.center();
  svgPanZoom.zoomBy(0.8);
}