import type { GraphStyleProperties } from "../types/GraphStyleProperties";
import type { Link } from "../types/Link";
import type { LinkStyle } from "../types/LinkStyle";
import type { Network } from "../types/Network";
import type { NodeStyle } from "../types/NodeStyle";
import type { Node } from "../types/Node";
import { stringToRGB } from "../utils/GraphUtils";

/**
 * Apply specific style to one link
 * @param link Link object
 * @param linkStyle LinkStyle object, must contain new style to apply
 * @param styleName Style name, use for define classe name
 * @param networkStyle GraphStyleProperties object
 */
export function addLinkStyle(link: Link, linkStyle: LinkStyle, styleName: string, networkStyle: GraphStyleProperties): void {
  link.classes?.push(styleName);
  if (networkStyle.linkStyles) {
    networkStyle.linkStyles[styleName] = linkStyle;
  } else {
    networkStyle['linkStyles'] = {};
    networkStyle.linkStyles[styleName] = linkStyle;
  }
}

/**
 * Remove specific style to one link
 * @param link Link object
 * @param styleName Style name, correspond to a specific style classe
 */
export function removeLinkStyle(link: Link, styleName: string): void {
  const classIndex = link.classes?.indexOf(styleName);
  if (classIndex && classIndex > -1) {
    link.classes?.splice(classIndex, 1);
  }
}

/**
 * Apply specific style to one node
 * @param node Node object
 * @param nodeStyle NodeStyle object, must contain new style to apply
 * @param styleName Style name, use for define classe name
 * @param networkStyle GraphStyleProperties object
 */
export function addNodeStyle(node: Node, nodeStyle: NodeStyle, styleName: string, networkStyle: GraphStyleProperties): void {
  node.classes?.push(styleName);
  if (networkStyle.nodeStyles) {
    networkStyle.nodeStyles[styleName] = nodeStyle;
  } else {
    networkStyle['nodeStyles'] = {};
    networkStyle.nodeStyles[styleName] = nodeStyle;
  }
}

/**
 * Remove specific style to one node
 * @param node Node object
 * @param styleName Style name, correspond to a specific style classe
 */
export function removeNodeStyle(node: Node, styleName: string): void {
  const classIndex = node.classes?.indexOf(styleName);
  if (classIndex && classIndex > -1) {
    node.classes?.splice(classIndex, 1);
  }
}

/**
 * Remove specific mapping style over network
 * @param network Network object
 * @param networkStyle Network style object
 * @param mappingName Mapping name
 * @param conditionName Condition name
 * @param type Type of mapping (identified, continuous or discrete)
 * @param style Style apply on
 */
export function removeMappingStyleOnNode(
  network: Network,
  networkStyle: GraphStyleProperties,
  mappingName: string,
  conditionName: string,
  type: string,
  style: string
): void {
  if (type === 'Continuous') {
    const styleName = 'BLOCK' + mappingName + '_' + conditionName + '_' + style + '_C';
    const completeStyleName = styleName.replace(' ', '');

    Object.keys(network.nodes).forEach((nodeID: string) => {
      network.nodes[nodeID].classes = network.nodes[nodeID].classes?.filter((className: string) => {
        if (!(className.includes(completeStyleName))) {
          return true;
        }
      });
    });

    if (networkStyle.nodeStyles) {
      Object.keys(networkStyle.nodeStyles).forEach((className: string) => {
        if (className.includes(completeStyleName) && networkStyle.nodeStyles) {
          delete networkStyle.nodeStyles[className];
        }
      });
    }
  }

  if (type === 'Discrete') {
    const styleName = 'BLOCK' + mappingName + '_' + conditionName + '_' + style + '_D';
    const completeStyleName = styleName.replace(' ', '');

    Object.keys(network.nodes).forEach((nodeID: string) => {
      network.nodes[nodeID].classes = network.nodes[nodeID].classes?.filter((className: string) => {
        if (!(className.includes(completeStyleName))) {
          return true;
        }
      });
    });

    if (networkStyle.nodeStyles) {
      Object.keys(networkStyle.nodeStyles).forEach((className: string) => {
        if (className.includes(completeStyleName) && networkStyle.nodeStyles) {
          delete networkStyle.nodeStyles[className];
        }
      });
    }
  }

  if (type === 'Identified in mapping') {
    const styleName = 'BLOCK' + mappingName + '_' + style + '_I';
    const completeStyleName = styleName.replace(' ', '');

    Object.keys(network.nodes).forEach((nodeID: string) => {
      network.nodes[nodeID].classes = network.nodes[nodeID].classes?.filter((className: string) => {
        if (!(className === completeStyleName)) {
          return true;
        }
      });
    });

    if (networkStyle.nodeStyles) {
      Object.keys(networkStyle.nodeStyles).forEach((className: string) => {
        if (className.includes(completeStyleName) && networkStyle.nodeStyles) {
          delete networkStyle.nodeStyles[className];
        }
      });
    }
  }
}

/**
 * Apply style on nodes according to mapping values
 * @param type Type of mapping: Discrete, Continuous or Identified in mapping
 * @param style Style to change for the mapping
 * @param targetLabel Target label to find nodes in network: id or label
 * @param values Mapping style values: Object, string or Function. According to mapping type
 * @param mappingName Mapping name
 * @param conditionName Condition name
 * @param data Object that contain id list and mapping values
 * @param network Network object
 * @param graphStyleProperties Network style object
 */

export function addMappingStyleOnNode(
  type: string,
  style: string, 
  targetLabel: string,
  values: Function | {[key: string]: string | number} | string, 
  mappingName: string,
  conditionName: string,
  data: {[key: string]: {[key: string]: string | number} | Array<string>},
  network: Network,
  graphStyleProperties: GraphStyleProperties
): void {

  let idList = data.id as Array<string>;
  if (targetLabel === "nodeLabel") {
    idList = findIdentifier(idList, network);
  }

  if (type === 'Discrete' && conditionName !== '') {
    applyDiscreteMapping(idList, data, values as {[key: string]: string | number}, network, style, mappingName, conditionName, graphStyleProperties);
  }

  if (type === 'Continuous' && conditionName !== '') {
    applyContinuousMapping(idList, data, values as Function, network, style, mappingName, conditionName, graphStyleProperties);
  }

  if (type === 'Identified in mapping') {
    applyIdentifiedMapping(idList, values as string, network, style, mappingName, graphStyleProperties);
  }
}

/**
 * Apply Discrete mapping style to nodes according to mapping values
 * @param idList List of nodes id
 * @param data Object that contain id list and mapping values
 * @param values Mapping style values: Object. According to mapping type Discrete
 * @param network Network object
 * @param style Style to change for the mapping
 * @param mappingName Mapping name
 * @param conditionName Condition name
 * @param graphStyleProperties Network style object
 */
function applyDiscreteMapping(
  idList: Array<string>, 
  data: {[key: string]: Array<string> | {[key: string]: number | string}}, 
  values: {[key: string]: string | number}, 
  network: Network, 
  style: string, 
  mappingName: string, 
  conditionName: string, 
  graphStyleProperties: GraphStyleProperties
): void {
  const styleName = 'BLOCK' + mappingName + '_' + conditionName + '_' + style + '_D';
  const completeStyleName = styleName.replace(' ', '');
  const mappingValue = data[conditionName] as {[key: string]: number};

  addMappingClassNameToNode(idList, completeStyleName, mappingValue, network);

  Object.keys(values).forEach((mapVal) => {
    const specificStyleName = completeStyleName + '?value:' + mapVal;
    addMappingClassObjectToStyle(style, values[mapVal], specificStyleName, graphStyleProperties);
  });
}

/**
 * Apply Continuous mapping style to nodes according to mapping values
 * @param idList List of nodes id
 * @param data Object that contain id list and mapping values
 * @param values Mapping style values: Function. According to mapping type Continuous
 * @param network Network object
 * @param style Style to change for the mapping
 * @param mappingName Mapping name
 * @param conditionName Condition name
 * @param graphStyleProperties Network style object
 */
function applyContinuousMapping(
  idList: Array<string>, 
  data: {[key: string]: Array<string> | {[key: string]: number | string}}, 
  values: Function, 
  network: Network, 
  style: string, 
  mappingName: string, 
  conditionName: string, 
  graphStyleProperties: GraphStyleProperties
): void {
  const styleName = 'BLOCK' + mappingName + '_' + conditionName + '_' + style + '_C';
  const completeStyleName = styleName.replace(' ', '');

  const mappingValue = data[conditionName] as {[key: string]: number};

  addMappingClassNameToNode(idList, completeStyleName, mappingValue, network);

  Object.keys(mappingValue).forEach((mapVal) => {
    const specificStyleName = completeStyleName + '?value:' + mappingValue[mapVal];
    addMappingClassObjectToStyle(style, values(mappingValue[mapVal]), specificStyleName, graphStyleProperties);
  });
}

/**
 * Apply Identified in mapping style to nodes according to mapping value
 * @param idList List of nodes id
 * @param values Mapping style values: string. According to mapping type Identified in mapping
 * @param network Network object
 * @param style Style to change for the mapping
 * @param mappingName Mapping name
 * @param graphStyleProperties Network style object
 */
function applyIdentifiedMapping(
  idList: Array<string>, 
  values: string, 
  network: Network, 
  style: string, 
  mappingName: string, 
  graphStyleProperties: GraphStyleProperties
): void {
  const styleName = 'BLOCK' + mappingName + '_' + style + '_I';
  const completeStyleName = styleName.replace(' ', '');
  addMappingClassNameToNode(idList, completeStyleName, undefined, network);
  addMappingClassObjectToStyle(style, values, completeStyleName, graphStyleProperties);
}

/**
 * Find nodes id from nodes label
 * @param idList List of nodes label
 * @param network Network object
 * @returns {Array<string>} List of nodes id
 */
function findIdentifier(idList: Array<string>, network: Network): Array<string> {
  const identifierList: Array<string> = [];

  Object.keys(network.nodes).forEach((node: string) => {
    const thisNode = network.nodes[node];
    if (thisNode.label) {
      if (idList.includes(thisNode.label)) {
        identifierList.push(node);
      }
    }
  });

  return identifierList;
}

/**
 * Add class name to nodes according to mapping values
 * @param idList List of nodes id
 * @param styleName Style name
 * @param mappingValue Mapping values
 * @param network Network object
 */
function addMappingClassNameToNode(idList: Array<string>, styleName: string, mappingValue: {[key: string]: number} | undefined, network: Network): void {
  let specificStyleName = styleName;
  idList.forEach((node) => {
    if (mappingValue) {
      specificStyleName = styleName + '?value:' + mappingValue[node];
    }
    if (network.nodes[node]) {
      if (network.nodes[node].classes) {
        network.nodes[node].classes?.push(specificStyleName);
      } else {
        network.nodes[node].classes = [specificStyleName];
      }
    }
  });
}

/**
 * Add class name to network style object according to mapping values
 * @param style Style to change
 * @param styleValue Style value
 * @param className Class name to add
 * @param graphStyleProperties Network style object
 */
function addMappingClassObjectToStyle(style: string, styleValue: number | string, className: string, graphStyleProperties: GraphStyleProperties): void {
  const newStyle: {[key: string]: string | number} = {};
  if (style === "Size") {
    if (typeof styleValue === 'string') {
      styleValue = parseFloat(styleValue);
    }
    newStyle['width'] = styleValue;
    newStyle['height'] = styleValue;
  }
  if (style === "Background color") {
    newStyle['fill'] = styleValue;
  }

  if (graphStyleProperties.nodeStyles) {
    graphStyleProperties.nodeStyles[className] = newStyle;
  }
}

/**
 * Color node border according to a metadata attribut. Must be a string
 * @param network Network object
 * @param networkStyle Network style object
 * @param attribut Attribut use for color
 */
export function nodeBorderColorByAttribut(network: Network, networkStyle: GraphStyleProperties, attribut: string): void {
  Object.keys(network.nodes).forEach((nodeID: string) => {
    const node = network.nodes[nodeID];
    if (node.metadata && node.metadata[attribut]) {
      const attCategory = node.metadata[attribut] as string;

      const styleName = 'BLOCK' + attCategory + 'borderColor';
      const specificStyleName = styleName.replace(' ', '');

      const borderColor = {
        'stroke': stringToRGB(attCategory)
      };

      if (node.classes) {
        node.classes.push(specificStyleName);
      } else {
        node.classes = [specificStyleName];
      }

      if (networkStyle.nodeStyles) {
        networkStyle.nodeStyles[specificStyleName] = borderColor;
      } else {
        networkStyle.nodeStyles = {};
        networkStyle.nodeStyles[specificStyleName] = borderColor;
      }
    }
  });
}

/**
 * Update style for a specific class
 * @param networkStyle Network style object
 * @param className Class name to modify
 * @param styleObject Style object. NodeStyle or LinkStyle
 * @param targetObject Target object. Link or node
 */
export function updateClassStyle(
  networkStyle: GraphStyleProperties,
  className: string,
  styleObject: NodeStyle | LinkStyle,
  targetObject: string
): void {
  if (targetObject === 'node') {
    if (networkStyle.nodeStyles) {
      networkStyle.nodeStyles[className] = styleObject;
    }
  } else {
    if (networkStyle.linkStyles) {
      networkStyle.linkStyles[className] = styleObject;
    }
  }
}

/**
 * Create new style class for a list of nodes or links
 * @param network Network object
 * @param networkStyle Network style object
 * @param className Class name to add
 * @param styleObject Style object. NodeStyle or LinkStyle
 * @param targetObject Target object. Link or node
 * @param listTarget List of target object id
 */
export function createClassStyle(
  network: Network, 
  networkStyle: GraphStyleProperties, 
  className: string, 
  styleObject: NodeStyle | LinkStyle,
  targetObject: string, 
  listTarget: Array<string>
): void {
  if (targetObject === 'node') {
    if (networkStyle.nodeStyles) {
      networkStyle.nodeStyles[className] = styleObject as NodeStyle;
    } else {
      networkStyle.nodeStyles = {};
      networkStyle.nodeStyles[className] = styleObject as NodeStyle;
    }

    Object.keys(network.nodes).forEach((nodeID: string) => {
      if (listTarget.includes(nodeID)) {
        if (network.nodes[nodeID].classes) {
          network.nodes[nodeID].classes?.unshift(className);
        } else {
          network.nodes[nodeID]['classes'] = [className];
        }
      }
    });
  } else {
    if (networkStyle.linkStyles) {
      networkStyle.linkStyles[className] = styleObject as LinkStyle;
    } else {
      networkStyle.linkStyles = {};
      networkStyle.linkStyles[className] = styleObject as LinkStyle;
    }

    network.links.forEach((link: Link) => {
      if (listTarget.includes(link.id)) {
        if (link.classes) {
          link.classes?.unshift(className);
        } else {
          link['classes'] = [className];
        }
      }
    })
  }
}