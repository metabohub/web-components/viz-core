import PolygonComponent from "../components/PolygonComponent.vue";
import CircleComponent from "../components/CircleComponent.vue";
import ImageComponent from "@/components/ImageComponent.vue";
import { Node } from "../types/Node";
import { NodeStyle } from "../types/NodeStyle";
import { Component } from "vue";

/**
 * According to node and nodeStyle data, return NodeShape object that contain Component to use and path or radius
 * @param node Node data
 * @param nodeStyle NodeStyle data
 * @returns {Component, string, number} NodeShape object
 */
export function getNodeShape(node: Node, nodeStyle: NodeStyle): {shape: Component, points?: string, radius?: number} {
  const height = nodeStyle.height ? nodeStyle.height : 20;
  const width = nodeStyle.width ? nodeStyle.width : 20;

  /**
   * Return path for a rect or square (according to height and width)
   */
  if (nodeStyle.shape === 'rect' || nodeStyle.shape === 'rectangle') {
    const initPos = node.x + ',' + node.y;
    const pos2 = node.x + ',' + (node.y + height);
    const pos3 = (node.x + width) + ',' + (node.y + height);
    const pos4 = (node.x + width) + ',' + node.y;

    return {
      shape: PolygonComponent,
      points: initPos + ' ' + pos2 + ' ' + pos3 + ' ' + pos4
    };
  } 

  /**
   * Return path for an inverse triangle
   */
  if (nodeStyle.shape === 'inverseTriangle') {
    const initPos = node.x + ',' + node.y;
    const pos2 = (node.x + width) + ',' + node.y;
    const pos3 = (node.x + (width / 2)) + ',' + (node.y + height);

    return {
      shape: PolygonComponent,
      points: initPos + ' ' + pos2 + ' ' + pos3
    };
  }

  /**
   * Return path for a triangle
   */
  if (nodeStyle.shape === 'triangle') {
    const initPos = node.x + ',' + (node.y + height);
    const pos2 = (node.x + width) + ',' + (node.y + height);
    const pos3 = (node.x + (width / 2)) + ',' + node.y;

    return {
      shape: PolygonComponent,
      points: initPos + ' ' + pos2 + ' ' + pos3
    }
  }

  /**
   * Return path for a diamond
   */
  if (nodeStyle.shape === 'diamond') {
    const initPos = (node.x - (width * 0.5)) + ',' + (node.y + (height * 0.5));
    const pos2 = (node.x + (width * 0.5)) + ',' + (node.y - height * 0.5);
    const pos3 = (node.x + (width * 1.5)) + ',' + (node.y + (height * 0.5));
    const pos4 = (node.x + (width * 0.5)) + ',' + (node.y + height * 1.5);

    return {
      shape: PolygonComponent,
      points: initPos + ' ' + pos2 + ' ' + pos3 + ' ' + pos4
    }
  } 

  /**
   * Return path for a rectangle image
   */
  if (nodeStyle.shape === 'image' || nodeStyle.shape === 'imageRect') {
    const initPos = node.x + ',' + node.y;
    const pos2 = node.x + ',' + (node.y + height);
    const pos3 = (node.x + width) + ',' + (node.y + height);
    const pos4 = (node.x + width) + ',' + node.y;

    return {
      shape: ImageComponent,
      points: initPos + ' ' + pos2 + ' ' + pos3 + ' ' + pos4
    }
  }

  /**
   * Return path for a rounded image
   */
  if (nodeStyle.shape === 'imageCircle') {
    return {
      shape: ImageComponent,
      radius: height / 2
    }
  }
  
  /**
   * Return radius for a circle
   */
  else {
    return {
      shape: CircleComponent,
      radius: height / 2
    }
  }
}