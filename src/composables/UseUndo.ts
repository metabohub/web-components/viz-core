import { useRefHistory } from "@vueuse/core";
import { cloneDeep } from 'lodash-es';

/**
 * Create different methods from useRefHistory to a specific object to manage undo / redo functions
 * @param objectUndoable object to assign undo / redo methods from useRefHistory library
 * @param capacityNumber number of actions to save
 * @returns { Function } functions to manage undo / redo for the specific object
 */
export function createUndoFunction(objectUndoable: any, capacityNumber: number): {undo: Function, redo: Function, commit: Function, resume: Function, pause: Function} {
  const { undo, redo, commit, resume, pause } = useRefHistory(objectUndoable, {
    clone: cloneDeep,
    capacity: capacityNumber
  });

  return { undo, redo, commit, resume, pause };
}