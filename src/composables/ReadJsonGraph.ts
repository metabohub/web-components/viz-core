import { v4 as uuidv4 } from 'uuid';
import type { GraphStyleProperties } from "../types/GraphStyleProperties";
import { Convexhulls, Network, Node } from "@/types";

/**
 * Read graph data and return network and styles object
 * @param jsonGraph JSONGraph object stringify
 * @returns {Network, GraphStyleProperties} Return network object and graphStyleProperties object
 */
export function readJsonGraph(jsonGraph: string): { network: Network, networkStyle: GraphStyleProperties, mappings: {[key: string]: any}, convexhulls: Convexhulls } {
	const jsonObject = JSON.parse(jsonGraph);

	const network: Network = {
		id: "",
		nodes: {},
		links: []
	};

	const networkStyle: GraphStyleProperties = {
		nodeStyles: {}
	}

	let mappings = {};

	let convexhulls = {} as Convexhulls

	if (!jsonObject.graph) {
		throw new Error("graph attribute lacking in json graph format")
	}

	if (!jsonObject.graph.nodes) {
		throw new Error("nodes attribute lacking in json graph format")
	}

	if (!jsonObject.graph.edges) {
		throw new Error("edges attribute lacking in json graph format")
	}

	if (jsonObject.graph.id) {
		network.id = jsonObject.graph.id;
	}
	else {
		network.id = uuidv4();
	}

	if (jsonObject.graph.label) {
		network.label = jsonObject.graph.label;
	}
	else {
		network.label = network.id;
	}

	if (jsonObject.graph.type) {
		network.type = jsonObject.graph.type;
	}

	Object.keys(jsonObject.graph.nodes).forEach((id: string) => {
		const nodeJSON = jsonObject.graph.nodes[id];
		const node: Node = {
			id: "",
			x: 0,
			y: 0,
			hidden: false
		};

		node.id = id.toString();

		if (!nodeJSON.label) {
			node.label = id.toString();
		}
		else {
			node.label = nodeJSON.label.toString();
		}

		if (node.id in network.nodes) {
			throw new Error("Duplicated node id : " + node.id);
		}

		if (nodeJSON.metadata) {
			node.metadata = nodeJSON.metadata;

			if (nodeJSON.metadata.classes) {
				node.classes = nodeJSON.metadata.classes;
			} else {
				node.classes = ['classicNode'];
			}

			if (nodeJSON.metadata.position && nodeJSON.metadata.position.x) {
				node.x = nodeJSON.metadata.position.x;
			}

			if (nodeJSON.metadata.position && nodeJSON.metadata.position.y) {
				node.y = nodeJSON.metadata.position.y;
			}
		} else {
			node.classes = ['classicNode'];
		}

		network.nodes[node.id] = node;
	});

	network.links = jsonObject.graph.edges.filter((link: { source: string, target: string, metadata: {[key: string]: string} }) => {
		if (network.nodes[link.source] && network.nodes[link.target]) {
			return true;
		}
	}).map((e: { source: string, target: string, metadata: {[key: string]: string} }) => {

		const source: Node = network.nodes[e.source];
		const target: Node = network.nodes[e.target];

		let classes;

		if (e.metadata) {
			if (e.metadata.classes) {
				classes = e.metadata.classes;
			} else {
				classes = ['classicEdge'];
			}
		} else {
			classes = ['classicEdge'];
		}

		return {
			...e,
			source: source,
			target: target,
			classes: classes,
			id: source.id + ' -- ' + target.id
		}
	});

	if (jsonObject.graph.metadata && jsonObject.graph.metadata.style) {
		if (jsonObject.graph.metadata.style.nodeStyles && Object.keys(jsonObject.graph.metadata.style.nodeStyles).length !== 0) {
			networkStyle.nodeStyles = jsonObject.graph.metadata.style.nodeStyles;
			if (Object.keys(network.nodes).length > 500) {
				Object.keys(networkStyle.nodeStyles).forEach((nodeStyle: string) => {
					networkStyle.nodeStyles![nodeStyle]['displayLabel'] = false;
				});
			}
		}

		if (!(jsonObject.graph.metadata.style.nodeStyles) || Object.keys(jsonObject.graph.metadata.style.nodeStyles).length === 0) {
			networkStyle.nodeStyles = {'classicNode': {}};
			if (Object.keys(network.nodes).length > 500) {
				Object.keys(networkStyle.nodeStyles).forEach((nodeStyle: string) => {
					networkStyle.nodeStyles![nodeStyle]['displayLabel'] = false;
				});
			}
		}

		if (jsonObject.graph.metadata.style.linkStyles && Object.keys(jsonObject.graph.metadata.style.linkStyles).length !== 0) {
			networkStyle.linkStyles = jsonObject.graph.metadata.style.linkStyles;
		}

		if (!(jsonObject.graph.metadata.style.linkStyles) || Object.keys(jsonObject.graph.metadata.style.linkStyles).length === 0) {
			networkStyle.linkStyles = {'classicEdge': {}};
		}

		if (jsonObject.graph.metadata.style.curveLine) {
			networkStyle.curveLine = jsonObject.graph.metadata.style.curveLine;
		}

		if (jsonObject.graph.directed) {
			networkStyle.directed = jsonObject.graph.directed;
		}
	}

	if (jsonObject.graph.metadata.mappings) {
		mappings = jsonObject.graph.metadata.mappings;
	}

	if (jsonObject.graph.metadata.convexhulls) {
		convexhulls = jsonObject.graph.metadata.convexhulls
	}

	return { network, networkStyle, mappings, convexhulls };

}

