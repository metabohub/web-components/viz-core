import { describe, test, expect } from 'vitest';

import type { Network } from '../../types/Network';
import type { GraphStyleProperties } from '../../types/GraphStyleProperties';

import { removeNode, removeAllSelectedNodes, duplicateNode } from '../UseManageNetworkData';
import { Link } from '../../types/Link';

const network: Network = {
  id: 'testNetwork',
  nodes: {
    "A": {
      id: "A",
      label: "A",
      x: 0,
      y: 0,
      selected: true
    },
    "B": {
      id: "B",
      label: "B",
      x: 10,
      y: 10,
      selected: true
    },
    "C": {
      id: "C",
      label: "C",
      x: 20,
      y: 20
    },
    "D": {
      id: "D",
      label: "D",
      x: 30,
      y: 30
    },
    "E": {
      id: "E",
      label: "E",
      x: 40,
      y: 40
    },
    "F": {
      id: "F",
      label: "F",
      x: 50,
      y: 50
    }
  },
  links: [
    {
      id: 'A - B',
      source: {
        id: "A",
        label: "A",
        x: 0,
        y: 0
      },
      target: {
        id: "B",
        label: "B",
        x: 10,
        y: 10
      }
    },
    {
      id: 'B - C',
      source: {
        id: "B",
        label: "B",
        x: 10,
        y: 10
      },
      target: {
        id: "C",
        label: "C",
        x: 20,
        y: 20
      }
    },
    {
      id: 'C - A',
      source: {
        id: "C",
        label: "C",
        x: 20,
        y: 20
      },
      target: {
        id: "A",
        label: "A",
        x: 0,
        y: 0
      }
    },
    {
      id: 'D - F',
      source: {
        id: "D",
        label: "D",
        x: 30,
        y: 30
      },
      target: {
        id: "F",
        label: "F",
        x: 50,
        y: 50
      }
    },
    {
      id: 'D - E',
      source: {
        id: "D",
        label: "D",
        x: 30,
        y: 30
      },
      target: {
        id: "E",
        label: "E",
        x: 40,
        y: 40
      }
    }
  ]
}

const networkStyle: GraphStyleProperties = {
  nodeStyles: {},
  linkStyles: {}
}

describe('Tests for UseManageNetworkData composable', () => {
  test('Test removeThisNode method', () => {
    const nodeId: string = 'C';
    removeNode(nodeId, network);

    expect(Object.keys(network.nodes).includes('C')).not.toBeTruthy();
    network.links.forEach((link: Link) => {
      expect(link.source.id).not.toEqual('C');
      expect(link.target.id).not.toEqual('C');
    });
  });

  test('Test removeAllSelectedNode method', () => {
    removeAllSelectedNodes(network);

    expect(Object.keys(network.nodes).includes('A')).not.toBeTruthy();
    expect(Object.keys(network.nodes).includes('B')).not.toBeTruthy();
    network.links.forEach((link: Link) => {
      expect(link.source.id).not.toEqual('A');
      expect(link.target.id).not.toEqual('A');

      expect(link.source.id).not.toEqual('B');
      expect(link.target.id).not.toEqual('B');
    });
  });


  test('Test duplicateThisNode method', async () => {
    const nodeId: string = 'D';
    await duplicateNode(nodeId, network, networkStyle)

    // Check if original node is removed
    console.log(network.nodes);
    expect(network.nodes['D']).toBeUndefined();
    network.links.forEach((link: Link) => {
      expect(link.source.id).not.toEqual('D');
      expect(link.target.id).not.toEqual('D');
    });

    // Check if new nodes are created
    expect(Object.keys(network.nodes).includes('D_copy-0')).toBeTruthy();
    expect(Object.keys(network.nodes).includes('D_copy-1')).toBeTruthy();

    let newLinksCount = 0;
    network.links.forEach((link: Link) => {
      if (
        link.source.id === 'D_copy-0' || 
        link.source.id === 'D_copy-1' ||
        link.target.id === 'D_copy-0' ||
        link.target.id === 'D_copy-1'
      ) {
        newLinksCount += 1;
      }
    });

    expect(newLinksCount).toEqual(2);
  });
});