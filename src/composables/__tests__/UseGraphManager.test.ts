/**
 * @vitest-environment jsdom
 */

import { describe, it, expect, beforeEach, vi } from 'vitest';
import { switchGraphMode, nodeSelection, defineBrush, stopBrush, switchCursor, verticalNodesAlign, horizontalNodesAlign, unselectAll } from '../UseGraphManager';
import type { Network } from '../../types/Network';
import type { Node } from '../../types/Node';
import type { NodeStyle } from '../../types/NodeStyle';

describe('UseGraphManager', () => {
  let mockSvgPanZoomInstance: any;
  let mockDivElement: HTMLElement;
  let mockSVGGElement: SVGGElement;

  beforeEach(() => {
    // Mock the SVG element
    mockDivElement = document.createElement('div');
    mockSVGGElement = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    
    // Mock svgPanZoom instance methods
    mockSvgPanZoomInstance = {
      updateBBox: vi.fn(),
      resize: vi.fn(),
      fit: vi.fn(),
      center: vi.fn(),
      destroy: vi.fn()
    };
  });

  it('should toggle graph mode', () => {
    expect(switchGraphMode(true)).toBe(false);
    expect(switchGraphMode(false)).toBe(true);
  });

  it('should select and unselect a node', () => {
    const node: Node = { id: 'node1', selected: false, x: 0, y: 0 };
    
    nodeSelection(node);
    expect(node.selected).toBe(true);

    nodeSelection(node);
    expect(node.selected).toBe(false);
  });

  it('should select a node without metadata and add metadata', () => {
    const node: Node = { id: 'node1', x: 0, y: 0 };

    nodeSelection(node);
    expect(node.selected).toBeTruthy();
  });

  it('should call d3 functions in defineBrush, switchCursor and stopBrush', () => {
    const network: Network = { nodes: {}, links: [], id: 'test' };
    const styles: {[key: string]: NodeStyle} = {};
    
    defineBrush(mockDivElement, mockSVGGElement, network, styles, mockSvgPanZoomInstance);
    switchCursor('crosshair', mockSVGGElement);
    stopBrush();
  });

  it('should vertically align selected nodes', () => {
    const network: Network = {
      nodes: {
        node1: { id: 'node1', x: 0, y: 0, selected: true },
        node2: { id: 'node2', x: 100, y: 100, selected: true },
      },
      links: [], id: 'test'
    };
    const styles: {[key: string]: NodeStyle} = {
      node1: { width: 50, height: 50 },
      node2: { width: 50, height: 50 },
    };

    verticalNodesAlign(network, styles);
    expect(network.nodes['node2'].x).toBe(0);
  });

  it('should horizontally align selected nodes', () => {
    const network: Network = {
      nodes: {
        node1: { id: 'node1', x: 0, y: 0, selected: true },
        node2: { id: 'node2', x: 100, y: 100, selected: true },
      },
      links: [], id: 'test'
    };
    const styles: {[key: string]: NodeStyle} = {
      node1: { width: 50, height: 50 },
      node2: { width: 50, height: 50 },
    };

    horizontalNodesAlign(network, styles);
    expect(network.nodes['node2'].y).toBe(0);
  });

  it('should unselect all nodes', () => {
    const network: Network = {
      nodes: {
        node1: { id: 'node1', x: 0, y: 0, selected: true },
        node2: { id: 'node2', x: 100, y: 100, selected: true },
      },
      links: [], id: 'test'
    };

    unselectAll(network);
    expect(network.nodes['node1'].selected).toBe(false);
    expect(network.nodes['node2'].selected).toBe(false);
  });
});