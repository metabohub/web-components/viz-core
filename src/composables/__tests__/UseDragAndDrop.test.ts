/**
 * @vitest-environment jsdom
 */

import { describe, it, expect, vi, beforeEach, SpyInstance } from 'vitest';
import * as DragAndDrop from '../UseDragAndDrop';
import CircleComponent from '../../components/CircleComponent.vue';
import { mount, VueWrapper } from '@vue/test-utils';

const mockProps = { node: { id: 'node1', x: 100, y: 200 }, shape: { radius: 15, shape: 'circle' }, style: {} };

describe('useDragAndDrop', () => {
  let select: SpyInstance;
  let NodeComponent: VueWrapper;

  beforeEach(() => {
    select = vi.spyOn(DragAndDrop, 'useDragAndDrop');
    NodeComponent = mount(CircleComponent, {
      props: mockProps,
    });
  });

  it('should initialize drag behavior on mounted', async () => {
    expect(NodeComponent.exists()).toBeTruthy();
    expect(select).toHaveBeenCalledWith(mockProps);
  });
});