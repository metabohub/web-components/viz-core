import { describe, it, expect, vi, beforeEach } from 'vitest';
import svgPanZoom from 'svg-pan-zoom';
import { initZoom, stopZoom, rescale } from '../UseZoomSvg';

vi.mock('svg-pan-zoom', () => ({
  default: vi.fn(),
}));

describe('UseZoomSvg', () => {
  let mockSvgPanZoomInstance: any;
  let mockSvgElement: SVGElement;
  let mockSVGGElement: SVGGElement;

  beforeEach(() => {
    // Mock the SVG element
    mockSvgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    mockSVGGElement = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    
    // Mock svgPanZoom instance methods
    mockSvgPanZoomInstance = {
      updateBBox: vi.fn(),
      resize: vi.fn(),
      fit: vi.fn(),
      center: vi.fn(),
      destroy: vi.fn(),
      zoomBy: vi.fn()
    };

    // Mock svgPanZoom function to return a mock instance
    (svgPanZoom as any).mockReturnValue(mockSvgPanZoomInstance);
  });

  describe('initZoom', () => {
    it('should initialize svgPanZoom with the correct parameters', () => {
      const instance = initZoom(mockSvgElement, mockSVGGElement);

      expect(svgPanZoom).toHaveBeenCalledWith(mockSvgElement, {
        dblClickZoomEnabled: false,
        viewportSelector: mockSVGGElement,
        zoomScaleSensitivity: 0.8,
        minZoom: 0,
        maxZoom: 1000,
        eventsListenerElement: mockSvgElement,
        customEventsHandler: expect.any(Object)
      });

      expect(instance).toBe(mockSvgPanZoomInstance);
    });
  });

  describe('stopZoom', () => {
    it('should return null when stopZoom is called', () => {
      const instance = initZoom(mockSvgElement, mockSVGGElement);

      stopZoom(instance);

      expect(mockSvgPanZoomInstance.destroy).toHaveBeenCalled();
    });
  });

  describe('rescale', () => {
    it('should call the svgPanZoom instance methods in the correct order', () => {
      rescale(mockSvgPanZoomInstance);

      expect(mockSvgPanZoomInstance.updateBBox).toHaveBeenCalled();
      expect(mockSvgPanZoomInstance.resize).toHaveBeenCalled();
      expect(mockSvgPanZoomInstance.fit).toHaveBeenCalled();
      expect(mockSvgPanZoomInstance.center).toHaveBeenCalled();
      expect(mockSvgPanZoomInstance.zoomBy).toHaveBeenCalled();
    });
  });
});