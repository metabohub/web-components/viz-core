/**
 * @vitest-environment jsdom
 */

import { describe, test, expect } from 'vitest';

import { createStaticForceLayout, createForceLayout, forceObject, addNewParams, applyParamsToForceLayout } from '../UseCreateForceLayout';
import { ForceParams } from '@/types';
import { ref } from 'vue';

const node1Value = {
  id: 'node1',
  x: 0,
  y: 0
};

const node2Value = {
  id: 'node2',
  x: 0,
  y: 0
};

const network = ref({
  id: 'network1',
  nodes: {
    node1: node1Value,
    node2: node2Value
  },
  links: [
    {
      id: 'link1',
      source: node1Value,
      target: node2Value
    }
  ]
});

const expectForceParams = {
  charge: {
    strength: -250,
    min: 1,
    max: 1000
  },
  collide: {
    strength: 15,
    radius: 15,
    iteration: 1
  },
  gravity: {
    strength: 6
  },
  link: {
    distance: 30,
    iteration: 1
  }
} as ForceParams

describe('Test UseCreateForceLayout composable methods', () => {
  test('forceObject', () => {
    expect(forceObject.simulation).toBeUndefined();
    expect(forceObject.isAnim).toBeFalsy();
    expect(forceObject.forceParams).toEqual(expectForceParams);
  });
  // test('createStaticForceLayout method', async () => {
  //   await createStaticForceLayout(network.value);
  //   expect(network.value.nodes.node1.x).not.toEqual(0);
  //   expect(network.value.nodes.node1.y).not.toEqual(0);

  //   expect(network.value.nodes.node2.x).not.toEqual(0);
  //   expect(network.value.nodes.node2.y).not.toEqual(0);

  //   expect(forceObject.simulation).toBeDefined();
  //   expect(forceObject.isAnim).toBeFalsy();
  // });

  // test('createForceLayout method', () => {
  //   createForceLayout(network.value);
    
  //   expect(network.value.nodes.node1.x).not.toEqual(0);
  //   expect(network.value.nodes.node1.y).not.toEqual(0);

  //   expect(network.value.nodes.node2.x).not.toEqual(0);
  //   expect(network.value.nodes.node2.y).not.toEqual(0);

  //   expect(forceObject.simulation).toBeDefined();
  //   expect(forceObject.isAnim).toBeTruthy();
  // });

  test('addNewParams', () => {
    const newForceParams = {
      charge: {
        strength: -20,
        min: 1,
        max: 1000
      },
      collide: {
        strength: 1,
        radius: 15,
        iteration: 1
      },
      gravity: {
        strength: 6
      },
      link: {
        distance: 300,
        iteration: 1
      }
    } as ForceParams

    addNewParams(newForceParams);

    expect(forceObject.forceParams).toEqual(newForceParams);
  });

  // test('applyNewParams', () => {
  //   applyParamsToForceLayout(network.value);
  //   expect(forceObject.simulation).toBeDefined();
  //   expect(forceObject.isAnim).toBeTruthy();
  // });
});