import { describe, it, expect } from 'vitest';
import {
  linePath,
  directedLinePath,
  reversibleDirectedLinePath,
  curvedLinePath,
} from '../UseLinePath';

describe('UseLinePath', () => {
  describe('linePath', () => {
    it('should create a straight line path between two nodes', () => {
      const sourceX = 100;
      const sourceY = 100;
      const targetX = 200;
      const targetY = 200;
      const sourceWidth = 50;
      const sourceHeight = 50;
      const targetWidth = 50;
      const targetHeight = 50;

      const expectedPath = 'M125,125L225,225';
      const result = linePath(
        sourceX,
        sourceY,
        targetX,
        targetY,
        sourceWidth,
        sourceHeight,
        targetWidth,
        targetHeight
      );

      expect(result).toBe(expectedPath);
    });

    it('should handle vertical lines correctly', () => {
      const sourceX = 100;
      const sourceY = 100;
      const targetX = 100;
      const targetY = 300;
      const sourceWidth = 50;
      const sourceHeight = 50;
      const targetWidth = 50;
      const targetHeight = 50;

      const expectedPath = 'M125,125L125,325';
      const result = linePath(
        sourceX,
        sourceY,
        targetX,
        targetY,
        sourceWidth,
        sourceHeight,
        targetWidth,
        targetHeight
      );

      expect(result).toBe(expectedPath);
    });

    it('should handle horizontal lines correctly', () => {
      const sourceX = 100;
      const sourceY = 100;
      const targetX = 300;
      const targetY = 100;
      const sourceWidth = 50;
      const sourceHeight = 50;
      const targetWidth = 50;
      const targetHeight = 50;

      const expectedPath = 'M125,125L325,125';
      const result = linePath(
        sourceX,
        sourceY,
        targetX,
        targetY,
        sourceWidth,
        sourceHeight,
        targetWidth,
        targetHeight
      );

      expect(result).toBe(expectedPath);
    });
  });

  describe('directedLinePath', () => {
    it('should create a directed line path with an arrowhead', () => {
      const sourceX = 100;
      const sourceY = 100;
      const targetX = 200;
      const targetY = 200;
      const sourceWidth = 50;
      const sourceHeight = 50;
      const targetWidth = 50;
      const targetHeight = 50;

      const expectedPath =
        'M125,125 L200.25126265847086,200.25126265847086 Q204.49390334559016,196.00862197135157 207.32233047033634,207.32233047033634 Q196.00862197135157,204.49390334559016 200.25126265847086,200.25126265847086 Z';
      const result = directedLinePath(
        sourceX,
        sourceY,
        targetX,
        targetY,
        sourceWidth,
        sourceHeight,
        targetWidth,
        targetHeight
      );

      expect(result).toBe(expectedPath);
    });

    it('should handle vertical directed lines correctly', () => {
      const sourceX = 100;
      const sourceY = 100;
      const targetX = 100;
      const targetY = 300;
      const sourceWidth = 50;
      const sourceHeight = 50;
      const targetWidth = 50;
      const targetHeight = 50;

      const expectedPath =
        'M125,125 L125,290 Q131,290 125,300 Q119,290 125,290 Z';
      const result = directedLinePath(
        sourceX,
        sourceY,
        targetX,
        targetY,
        sourceWidth,
        sourceHeight,
        targetWidth,
        targetHeight
      );

      expect(result).toBe(expectedPath);
    });

    it('should handle horizontal directed lines correctly', () => {
      const sourceX = 100;
      const sourceY = 100;
      const targetX = 300;
      const targetY = 100;
      const sourceWidth = 50;
      const sourceHeight = 50;
      const targetWidth = 50;
      const targetHeight = 50;

      const expectedPath =
        'M125,125 L290,125 Q290,119 300,125 Q290,131 290,125 Z';
      const result = directedLinePath(
        sourceX,
        sourceY,
        targetX,
        targetY,
        sourceWidth,
        sourceHeight,
        targetWidth,
        targetHeight
      );

      expect(result).toBe(expectedPath);
    });
  });

  describe('reversibleDirectedLinePath', () => {
    it('should create a reversible directed line path with arrowheads at both ends', () => {
      const sourceX = 100;
      const sourceY = 100;
      const targetX = 200;
      const targetY = 200;
      const sourceWidth = 50;
      const sourceHeight = 50;
      const targetWidth = 50;
      const targetHeight = 50;

      const expectedPath =
        'M125,125L192.92893218813452,192.92893218813452L198.5857864376269,194.34314575050763L194.34314575050763,198.5857864376269L200,200L198.5857864376269,194.34314575050763L194.34314575050763,198.5857864376269L192.92893218813452,192.92893218813452';
      const result = reversibleDirectedLinePath(
        sourceX,
        sourceY,
        targetX,
        targetY,
        sourceWidth,
        sourceHeight,
        targetWidth,
        targetHeight
      );

      expect(result).toBe(expectedPath);
    });

    it('should handle vertical reversible directed lines correctly', () => {
      const sourceX = 100;
      const sourceY = 100;
      const targetX = 100;
      const targetY = 300;
      const sourceWidth = 50;
      const sourceHeight = 50;
      const targetWidth = 50;
      const targetHeight = 50;

      const expectedPath =
        'M125,125L125,290L128,295L122,295L125,300L128,295L122,295L125,290';
      const result = reversibleDirectedLinePath(
        sourceX,
        sourceY,
        targetX,
        targetY,
        sourceWidth,
        sourceHeight,
        targetWidth,
        targetHeight
      );

      expect(result).toBe(expectedPath);
    });

    it('should handle horizontal reversible directed lines correctly', () => {
      const sourceX = 100;
      const sourceY = 100;
      const targetX = 300;
      const targetY = 100;
      const sourceWidth = 50;
      const sourceHeight = 50;
      const targetWidth = 50;
      const targetHeight = 50;

      const expectedPath =
        'M125,125L290,125L295,122L295,128L300,125L295,122L295,128L290,125';
      const result = reversibleDirectedLinePath(
        sourceX,
        sourceY,
        targetX,
        targetY,
        sourceWidth,
        sourceHeight,
        targetWidth,
        targetHeight
      );

      expect(result).toBe(expectedPath);
    });
  });

  describe('curvedLinePath', () => {
    it('should create a curved line path when nodes are horizontally aligned', () => {
      const sourceX = 100;
      const sourceY = 100;
      const targetX = 300;
      const targetY = 100;
      const sourceWidth = 50;
      const sourceHeight = 50;
      const targetWidth = 50;
      const targetHeight = 50;

      const expectedPath =
        'M125,125L160,125Q230,125,230,125Q230,125,295,125L300,125';
      const result = curvedLinePath(
        sourceX,
        sourceY,
        targetX,
        targetY,
        sourceWidth,
        sourceHeight,
        targetWidth,
        targetHeight
      );

      expect(result).toBe(expectedPath);
    });

    it('should create a curved line path when nodes are vertically aligned', () => {
      const sourceX = 100;
      const sourceY = 100;
      const targetX = 100;
      const targetY = 300;
      const sourceWidth = 50;
      const sourceHeight = 50;
      const targetWidth = 50;
      const targetHeight = 50;

      const expectedPath =
        'M125,125L125,160Q125,230,125,230Q125,230,125,295L125,300';
      const result = curvedLinePath(
        sourceX,
        sourceY,
        targetX,
        targetY,
        sourceWidth,
        sourceHeight,
        targetWidth,
        targetHeight
      );

      expect(result).toBe(expectedPath);
    });

    it('should create a curved line path when nodes are diagonally aligned', () => {
      const sourceX = 100;
      const sourceY = 100;
      const targetX = 200;
      const targetY = 300;
      const sourceWidth = 50;
      const sourceHeight = 50;
      const targetWidth = 50;
      const targetHeight = 50;

      const expectedPath =
        'M125,125L125,160Q125,325,195,325L200,325';
      const result = curvedLinePath(
        sourceX,
        sourceY,
        targetX,
        targetY,
        sourceWidth,
        sourceHeight,
        targetWidth,
        targetHeight
      );

      expect(result).toBe(expectedPath);
    });

    it('should handle overlapping nodes gracefully', () => {
      const sourceX = 150;
      const sourceY = 150;
      const targetX = 150;
      const targetY = 150;
      const sourceWidth = 50;
      const sourceHeight = 50;
      const targetWidth = 50;
      const targetHeight = 50;

      const expectedPath =
        'M175,175L175,140C175,125,175,125,175,145L175,150';
      const result = curvedLinePath(
        sourceX,
        sourceY,
        targetX,
        targetY,
        sourceWidth,
        sourceHeight,
        targetWidth,
        targetHeight
      );

      expect(result).toBe(expectedPath);
    });

    it('should handle nodes with different sizes', () => {
      const sourceX = 50;
      const sourceY = 50;
      const targetX = 250;
      const targetY = 150;
      const sourceWidth = 30;
      const sourceHeight = 60;
      const targetWidth = 70;
      const targetHeight = 40;

      const expectedPath =
        'M65,80L110,80Q285,80,285,145L285,150';
      const result = curvedLinePath(
        sourceX,
        sourceY,
        targetX,
        targetY,
        sourceWidth,
        sourceHeight,
        targetWidth,
        targetHeight
      );

      expect(result).toBe(expectedPath);
    });
  });
});