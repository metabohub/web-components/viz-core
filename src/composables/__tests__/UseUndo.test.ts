import { createUndoFunction } from '../UseUndo';
import { describe, it, expect, vi, beforeEach } from 'vitest';
import { useRefHistory } from "@vueuse/core";
import { cloneDeep } from 'lodash-es';

// Mock the useRefHistory function from @vueuse/core
vi.mock('@vueuse/core', () => ({
  useRefHistory: vi.fn(),
}));

// Mock cloneDeep to return a simple deep clone (optional, depends on your needs)
vi.mock('lodash-es', () => ({
  cloneDeep: vi.fn((obj) => JSON.parse(JSON.stringify(obj))),
}));

describe('createUndoFunction', () => {
  let undoMock: ReturnType<typeof vi.fn>;
  let redoMock: ReturnType<typeof vi.fn>;
  let commitMock: ReturnType<typeof vi.fn>;
  let resumeMock: ReturnType<typeof vi.fn>;
  let pauseMock: ReturnType<typeof vi.fn>;

  beforeEach(() => {
    // Reset the mock functions before each test
    undoMock = vi.fn();
    redoMock = vi.fn();
    commitMock = vi.fn();
    resumeMock = vi.fn();
    pauseMock = vi.fn();

    // Set up the useRefHistory mock to return our mock functions
    (useRefHistory as ReturnType<typeof vi.fn>).mockReturnValue({
      undo: undoMock,
      redo: redoMock,
      commit: commitMock,
      resume: resumeMock,
      pause: pauseMock,
    });
  });

  it('should return an object with undo, redo, commit, resume, and pause functions', () => {
    const objectUndoable = { someKey: 'someValue' };
    const capacityNumber = 10;

    const result = createUndoFunction(objectUndoable, capacityNumber);

    expect(result).toHaveProperty('undo');
    expect(result).toHaveProperty('redo');
    expect(result).toHaveProperty('commit');
    expect(result).toHaveProperty('resume');
    expect(result).toHaveProperty('pause');
  });

  it('should call useRefHistory with the correct arguments', () => {
    const objectUndoable = { someKey: 'someValue' };
    const capacityNumber = 10;

    createUndoFunction(objectUndoable, capacityNumber);

    expect(useRefHistory).toHaveBeenCalledWith(objectUndoable, {
      clone: cloneDeep,
      capacity: capacityNumber
    });
  });

  it('should call the undo function when undo is called', () => {
    const objectUndoable = { someKey: 'someValue' };
    const { undo } = createUndoFunction(objectUndoable, 10);

    undo();

    expect(undoMock).toHaveBeenCalled();
  });

  it('should call the redo function when redo is called', () => {
    const objectUndoable = { someKey: 'someValue' };
    const { redo } = createUndoFunction(objectUndoable, 10);

    redo();

    expect(redoMock).toHaveBeenCalled();
  });

  it('should call the commit function when commit is called', () => {
    const objectUndoable = { someKey: 'someValue' };
    const { commit } = createUndoFunction(objectUndoable, 10);

    commit();

    expect(commitMock).toHaveBeenCalled();
  });

  it('should call the resume function when resume is called', () => {
    const objectUndoable = { someKey: 'someValue' };
    const { resume } = createUndoFunction(objectUndoable, 10);

    resume();

    expect(resumeMock).toHaveBeenCalled();
  });

  it('should call the pause function when pause is called', () => {
    const objectUndoable = { someKey: 'someValue' };
    const { pause } = createUndoFunction(objectUndoable, 10);

    pause();

    expect(pauseMock).toHaveBeenCalled();
  });
});