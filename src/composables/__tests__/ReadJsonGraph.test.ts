import { describe, test, expect } from 'vitest';

import { readJsonGraph } from '../ReadJsonGraph';

const jsonGraph = {
  "graph": {
    "metadata": {
      "style": {
        "nodeStyles": {
          "metabolite": {
            "width": 10,
            "height": 10,
            "strokeWidth": 1,
            "shape": "circle"
          },
          "reaction": {
            "width": 15,
            "height": 10,
            "strokeWidth": 1,
            "shape": "rect"
          },
          "molecule": {
            "width": 25,
			"height": 10,
            "shape": "molecule",
			"strokeWidth": 0.5,
			"fill": "white",
			"stroke": "grey"
          }
        }
      }
    },
    "id": "mini metabolic network",
    "type": "metabolic",
    "nodes": {
      "A": {
        "metadata": {
          "smiles": "CCC",
          "classes": ["molecule"],
          "position": {
            "x": 10,
            "y": 100
          }
        },
        "label": "A"
      },
      "B": {
        "metadata": {
          "smiles": "C=C",
          "classes": ["molecule"],
          "position": {
            "x": 80,
            "y": 100
          }
        },
        "label": "B"
      },
      "C": {
        "metadata": {
          "classes": ["metabolite"],
          "position": {
            "x": 160,
            "y": 100
          }
        },
        "label": "C"
      },
      "R1": {
        "metadata": {
          "classes": ["reaction"],
          "position": {
            "x": 40,
            "y": 100
          }
        },
        "label": "R1"
      },
      "R2": {
        "metadata": {
          "classes": ["reaction"],
          "position": {
            "x": 120,
            "y": 100
          }
        },
        "label": "R2"
      }
    },
    "edges": [
      {
        "source": "A",
        "target": "R1"
      },
      {
        "source": "R1",
        "target": "B"
      },
      {
        "source": "B",
        "target": "R2"
      },
      {
        "source": "R2",
        "target": "C"
      }
    ]
  }
}

describe('Test ReadJsonGraph composable method', () => {
  test('readJsonGraph method', () => {
    const graphData = readJsonGraph(JSON.stringify(jsonGraph));
    
    const nodesList = graphData.network.nodes;
    const linksList = graphData.network.links;

    const style = graphData.networkStyle;
    
    expect(nodesList).toBeTruthy();
    expect(Object.keys(nodesList).length).toEqual(5);
    expect(Object.keys(nodesList).includes('A')).toBeTruthy();
    expect(Object.keys(nodesList).includes('AAAA')).not.toBeTruthy();

    expect(linksList).toBeTruthy();
    expect(linksList.length).toEqual(4);

    expect(style).toBeTruthy();
    expect(style.nodeStyles?.metabolite).toBeTruthy();
    expect(style.nodeStyles?.reaction).toBeTruthy();
  });
});