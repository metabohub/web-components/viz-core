import { describe, test, expect } from "vitest";

import type { Node } from "../../types/Node";
import type { NodeStyle } from "../../types/NodeStyle";

import { getNodeShape } from "../UsePolygonShape";

import CircleComponent from "../../components/CircleComponent.vue";
import PolygonComponent from "../../components/PolygonComponent.vue";

const circleStyle: NodeStyle = {
  "width": 25,
  "height": 25,
  "strokeWidth": 1,
  "shape": "circle"
};

const rectStyle: NodeStyle = {
  "width": 25,
  "height": 25,
  "strokeWidth": 1,
  "shape": "rect"
};

const triangleStyle: NodeStyle = {
  "width": 25,
  "height": 25,
  "shape": "triangle",
  "strokeWidth": 1,
  "fill": "white",
  "stroke": "grey"
};

const diamondStyle: NodeStyle = {
  "shape": "diamond",
  "width": 25,
  "height": 25,
  "strokeWidth": 1
};

const inverseTriangleStyle: NodeStyle = {
  "shape": "inverseTriangle",
  "width": 25,
  "height": 25,
  "strokeWidth": 1
};

const node: Node = {
  "id": "A",
  "x": 10,
  "y": 100,
  "metadata": {
    "smiles": "CCC",
    "position": {
      "x": 10,
      "y": 100
    }
  },
  "label": "A"
};

describe('Test UsePolygonShape composable', () => {
  test('Test getNodeShape method', () => {
    const circleNode = getNodeShape(node, circleStyle);

    expect(circleNode.shape).toBe(CircleComponent);
    expect(circleNode.radius).toEqual(12.5);

    const rectNode = getNodeShape(node, rectStyle);

    expect(rectNode.shape).toBe(PolygonComponent);
    expect(rectNode.points).toEqual('10,100 10,125 35,125 35,100');

    const inverseTriangleNode = getNodeShape(node, inverseTriangleStyle);

    expect(inverseTriangleNode.shape).toBe(PolygonComponent);
    expect(inverseTriangleNode.points).toEqual('10,100 35,100 22.5,125');

    const triangleNode = getNodeShape(node, triangleStyle);

    expect(triangleNode.shape).toBe(PolygonComponent);
    expect(triangleNode.points).toEqual('10,125 35,125 22.5,100');

    const diamondNode = getNodeShape(node, diamondStyle);

    expect(diamondNode.shape).toBe(PolygonComponent);
    expect(diamondNode.points).toEqual('-2.5,112.5 22.5,87.5 47.5,112.5 22.5,137.5');
  });
});