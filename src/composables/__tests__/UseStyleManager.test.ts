import { describe, it, expect } from 'vitest';
import {
  addLinkStyle,
  removeLinkStyle,
  addNodeStyle,
  removeNodeStyle,
  addMappingStyleOnNode,
  removeMappingStyleOnNode,
  nodeBorderColorByAttribut,
  updateClassStyle,
  createClassStyle
} from '../UseStyleManager';

import type { Link } from '../../types/Link';
import type { Node } from '../../types/Node';
import type { GraphStyleProperties } from '../../types/GraphStyleProperties';
import type { LinkStyle } from '../../types/LinkStyle';
import type { NodeStyle } from '../../types/NodeStyle';
import type { Network } from '../../types/Network';

const node1Value = {
  id: 'node1',
  x: 0,
  y: 0,
  classes: [],
  metadata: {
    compartment: 'cytoplasm'
  }
};

const node2Value = {
  id: 'node2',
  x: 0,
  y: 0,
  classes: [],
  metadata: {
    compartment: 'cytoplasm'
  }
};

const network: Network = {
  id: 'network1',
  nodes: {
    node1: node1Value,
    node2: node2Value
  },
  links: [
    {
      id: 'link1',
      source: node1Value,
      target: node2Value
    }
  ]
};

const networkStyle: GraphStyleProperties = {
  nodeStyles: {

  },
  linkStyles: {

  }
}

describe('UseStyleManager', () => {
  describe('addLinkStyle', () => {
    it('should add a style to a link and update the network style', () => {
      const nodes = {
        A: {
          id: 'A',
          x: 0,
          y: 0
        },
        B: {
          id: 'B',
          x: 10,
          y: 10
        }
      }

      const link: Link = { id: '1', classes: [], source: nodes.A, target: nodes.B };
      const linkStyle: LinkStyle = { stroke: 'red', strokeWidth: 2 };
      const styleName = 'newStyle';

      addLinkStyle(link, linkStyle, styleName, networkStyle);

      expect(link.classes).toContain(styleName);
      expect(networkStyle.linkStyles?.[styleName]).toEqual(linkStyle);
    });
  });

  describe('removeLinkStyle', () => {
    it('should remove a style from a link', () => {
      const nodes = {
        A: {
          id: 'A',
          x: 0,
          y: 0
        },
        B: {
          id: 'B',
          x: 10,
          y: 10
        }
      }

      const link: Link = { id: '1', classes: ['existingStyle', 'toRemove'], source: nodes.A, target: nodes.B };
      const styleName = 'toRemove';

      removeLinkStyle(link, styleName);

      expect(link.classes).not.toContain(styleName);
      expect(link.classes).toContain('existingStyle');
    });
  });

  describe('addNodeStyle', () => {
    it('should add a style to a node and update the network style', () => {
      const node: Node = { id: '1', classes: [], x: 0, y: 0 };
      const nodeStyle: NodeStyle = { fill: 'blue', shape: 'circle' };
      const styleName = 'newNodeStyle';

      addNodeStyle(node, nodeStyle, styleName, networkStyle);

      expect(node.classes).toContain(styleName);
      expect(networkStyle.nodeStyles?.[styleName]).toEqual(nodeStyle);
    });
  });

  describe('removeNodeStyle', () => {
    it('should remove a style from a node', () => {
      const node: Node = { id: '1', classes: ['style1', 'toRemove'], x: 0, y: 0 };
      const styleName = 'toRemove';

      removeNodeStyle(node, styleName);

      expect(node.classes).not.toContain(styleName);
      expect(node.classes).toContain('style1');
    });
  });

  describe('addMappingStyleOnNode', () => {
    it('should apply discrete mapping style to nodes', () => {
      const data = { id: ['node1', 'node2'], color: { node1: '10', node2: '20' } };
      const values = { 10: '#ff0000', 20: '#0000ff' };
      const style = 'Background color';
      const type = 'Discrete';
      const mappingName = 'colorMapping';
      const conditionName = 'color';

      addMappingStyleOnNode(type, style, 'id', values, mappingName, conditionName, data, network, networkStyle);

      const styleClassNode1 = 'BLOCKcolorMapping_color_Backgroundcolor_D?value:10';
      const styleClassNode2 = 'BLOCKcolorMapping_color_Backgroundcolor_D?value:20';

      expect(network.nodes['node1'].classes).toContain(styleClassNode1);
      expect(network.nodes['node2'].classes).toContain(styleClassNode2);
      expect(networkStyle.nodeStyles?.[styleClassNode1]?.fill).toBe('#ff0000');
      expect(networkStyle.nodeStyles?.[styleClassNode2]?.fill).toBe('#0000ff');
    });
  });

  describe('removeMappingStyleOnNode', () => {
    it('should remove mapping style from nodes', () => {
      const style = 'Background color';
      const mappingName = 'colorMapping';
      const conditionName = 'color';
      const type = 'Discrete';

      network.nodes['node1'].classes = ['BLOCKcolorMapping_color_Backgroundcolor_D?value:red'];
      networkStyle.nodeStyles = {
        'BLOCKcolorMapping_color_Backgroundcolor_D?value:red': { fill: 'red' },
      };

      removeMappingStyleOnNode(network, networkStyle, mappingName, conditionName, type, style);

      expect(network.nodes['node1'].classes).not.toContain('BLOCKcolorMapping_color_Backgroundcolor_D?value:red');
      expect(networkStyle.nodeStyles?.['BLOCKcolorMapping_color_Backgroundcolor_D?value:red']).toBeUndefined();
    });
  });

  describe('nodeBorderColorByAttribut', () => {
    it('should color node borders based on metadata attribute', () => {
      nodeBorderColorByAttribut(network, networkStyle, 'compartment');
      const styleName = 'BLOCKcytoplasmborderColor';
      expect(network.nodes['node1'].classes).toContain(styleName);
      expect(networkStyle.nodeStyles?.[styleName]?.stroke).toBe('#02D39D');
    });
  });

  describe('updateClassStyle', () => {
    it('should update the style for a specific class', () => {
      const className = 'existingClass';
      const styleObject: NodeStyle = { fill: 'green' };
      networkStyle.nodeStyles = { existingClass: { fill: 'red' } };

      updateClassStyle(networkStyle, className, styleObject, 'node');

      expect(networkStyle.nodeStyles?.[className]).toEqual(styleObject);
    });
  });

  describe('createClassStyle', () => {
    it('should create a new style class for nodes', () => {
      const className = 'newClass';
      const styleObject: NodeStyle = { fill: 'purple' };
      const listTarget = ['node1'];

      createClassStyle(network, networkStyle, className, styleObject, 'node', listTarget);

      expect(network.nodes['node1'].classes).toContain(className);
      expect(networkStyle.nodeStyles?.[className]).toEqual(styleObject);
    });

    it('should create a new style class for links', () => {
      const className = 'linkClass';
      const styleObject: LinkStyle = { stroke: 'yellow' };
      const listTarget = ['link1'];

      createClassStyle(network, networkStyle, className, styleObject, 'link', listTarget);

      expect(network.links[0].classes).toContain(className);
      expect(networkStyle.linkStyles?.[className]).toEqual(styleObject);
    });
  });
});