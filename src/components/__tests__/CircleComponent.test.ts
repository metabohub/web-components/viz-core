/**
 * @vitest-environment jsdom
 */

import { mount, VueWrapper } from '@vue/test-utils';
import { describe, it, expect, beforeEach } from 'vitest';
import CircleComponent from '../CircleComponent.vue';

describe('CircleComponent.vue', () => {
  let wrapper: VueWrapper;

  const mockNode = {
    id: 'node1',
    x: 100,
    y: 200,
    label: 'Node 1',
  };

  const mockStyle = {
    fill: 'red',
    stroke: 'black',
    strokeWidth: 2,
    opacity: 0.8,
    width: 20,
    height: 20,
  };

  const mockShape = {
    radius: 10,
    shape: 'circle'
  };

  beforeEach(() => {
    wrapper = mount(CircleComponent, {
      props: {
        node: mockNode,
        style: mockStyle,
        shape: mockShape,
      },
    });
  });

  it('renders the circle with correct attributes', () => {
    const circle = wrapper.find('circle');
    expect(circle.exists()).toBe(true);
    expect(circle.attributes('id')).toBe(mockNode.id);
    expect(circle.attributes('cx')).toBe(String(100 + mockStyle.width / 2)); // computed X
    expect(circle.attributes('cy')).toBe(String(200 + mockStyle.height / 2)); // computed Y
    expect(circle.attributes('r')).toBe(String(mockShape.radius)); // radius
    expect(circle.attributes('fill')).toBe(mockStyle.fill); // fill color
    expect(circle.attributes('stroke')).toBe(mockStyle.stroke); // stroke color
    expect(circle.attributes('stroke-width')).toBe(String(mockStyle.strokeWidth)); // stroke width
    expect(circle.attributes('opacity')).toBe(String(mockStyle.opacity)); // opacity
    expect(circle.attributes('name')).toBe(mockNode.label); // label
  });

  it('computes correct circle X position', () => {
    const circle = wrapper.find('circle');
    const expectedX = 100 + mockStyle.width / 2;
    expect(circle.attributes('cx')).toBe(String(expectedX));
  });

  it('computes correct circle Y position', () => {
    const circle = wrapper.find('circle');
    const expectedY = 200 + mockStyle.height / 2;
    expect(circle.attributes('cy')).toBe(String(expectedY));
  });

  it('applies cursor style from useDragAndDrop composable', () => {
    const circle = wrapper.find('circle');
    // Since `cursor` is a computed value, you can mock its value or inspect the rendered style attribute
    expect(circle.attributes('style')).toBeDefined(); // Check if cursor style is applied
  });
});