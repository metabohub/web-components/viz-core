/**
 * @vitest-environment jsdom
 */

import { mount, VueWrapper } from '@vue/test-utils';
import { describe, it, expect, beforeEach } from 'vitest';
import LinkData from '../LinkData.vue';
import { LinkStyle } from '../../types/LinkStyle';
import { NodeStyle } from '../../types/NodeStyle';

describe('LinkData.vue', () => {
  let wrapper: VueWrapper;

  const mockLink = {
    source: { id: '1', x: 100, y: 100, hidden: false, classes: ['metabolite'] },
    target: { id: '2', x: 200, y: 200, hidden: false, classes: ['reaction'] },
    id: 'link1',
    classes: ['classicEdge']
  };

  const mockLinkStyle: {[key: string]: LinkStyle} = {
    classicEdge: {
      stroke: 'blue',
      strokeWidth: 2,
      opacity: 0.8,
      display: true,
    }
  };

  const mockNodeStyle: {[key: string]: NodeStyle} = {
    reaction: {
      width: 10,
      height: 10,
    },
    metabolite: {
      width: 10,
      height: 10,
    }
  };

  beforeEach(() => {
    wrapper = mount(LinkData, {
      props: {
        link: mockLink,
        linkStyle: mockLinkStyle,
        nodeStyle: mockNodeStyle,
        lineStyle: false,
        directed: false,
      },
    });
  });

  it('renders the SVG path with correct attributes when both source and target are visible', () => {
    const path = wrapper.find('path');
    expect(path.exists()).toBe(true);
    expect(path.attributes('d')).toBe('M105,105L205,205');
    expect(path.attributes('stroke')).toBe('blue');
    expect(path.attributes('stroke-width')).toBe('2');
    expect(path.attributes('opacity')).toBe('0.8');
  });

  it('hides the link when the source node is hidden', async () => {
    await wrapper.setProps({
      link: { ...mockLink, source: { ...mockLink.source, hidden: true } },
    });
    expect(wrapper.find('path').exists()).toBe(false);
  });

  it('hides the link when the target node is hidden', async () => {
    await wrapper.setProps({
      link: { ...mockLink, target: { ...mockLink.target, hidden: true } },
    });
    expect(wrapper.find('path').exists()).toBe(false);
  });

  // it('computes the correct link path based on source and target node styles', async () => {
  //   const mockTransformAttribute = vi.spyOn(GraphLink, "transformAttribute");
  //   const sourceStyle = getNodeStyle(mockLink.source, mockNodeStyle);
  //   const targetStyle = getNodeStyle(mockLink.target, mockNodeStyle);

  //   await wrapper.vm.$nextTick();

  //   expect(mockTransformAttribute).toHaveBeenCalledWith(
  //     false,
  //     false,
  //     mockLink.source,
  //     mockLink.target,
  //     sourceStyle,
  //     targetStyle,
  //   );
  // });

  // it('computes the correct link style using getLinkStyle', () => {
  //   const getLinkStyleMock = vi.spyOn(LinkStyles, 'getLinkStyle');
  //   expect(getLinkStyleMock).toHaveBeenCalledWith(mockLink, mockLinkStyle);
  // });

  // it('computes the correct source and target node styles using getNodeStyle', () => {
  //   expect(getNodeStyle).toHaveBeenCalledWith(mockLink.source, { default: mockNodeStyle });
  //   expect(getNodeStyle).toHaveBeenCalledWith(mockLink.target, { default: mockNodeStyle });
  // });
});