/**
 * @vitest-environment jsdom
 */

import { mount, VueWrapper } from '@vue/test-utils';
import { describe, it, expect, beforeEach } from 'vitest';
import NodeData from '../NodeData.vue';
import CircleComponent from '../CircleComponent.vue';
import PolygonComponent from '../PolygonComponent.vue';
import { ComponentPublicInstance } from 'vue';
import { NodeShape } from '../../types/NodeShape';
import { NodeStyle } from '../../types/NodeStyle';

const node = {
  id: '1',
  x: 100,
  y: 200,
  label: 'Node 1',
  hidden: false,
  classes: ['class1'],
  metadata: {
    selected: true,
  },
};

const style = {
  'class1': {
    width: 50,
    height: 50,
    fill: 'red',
    label: 'name',
    displayLabel: true,
    shape: 'circle'
  },
  'class2': {
    width: 50,
    height: 50,
    fill: 'red',
    label: 'name',
    displayLabel: true,
    shape: 'rectangle'
  }
};

describe('NodeData.vue', () => {
  type componentProps = any;
  type componentVariables = {
    labelX: number,
    labelY: number,
    nodeShape: NodeShape,
    nodeStyle: NodeStyle
  }
  let wrapper: VueWrapper<ComponentPublicInstance<componentProps, componentVariables>>;

  beforeEach(() => {
    wrapper = mount(NodeData, {
      props: {
        node,
        style,
      },
    });
  });

  it('renders the node shape and label correctly', async () => {
    const nodeShape = wrapper.findComponent(CircleComponent);
    expect(nodeShape.exists()).toBe(true);
    expect(nodeShape.attributes('class')).toContain('node');

    // Check label rendering
    const textElement = wrapper.find('text');
    expect(textElement.exists()).toBe(true);
    expect(textElement.text()).toBe('Node 1');

    await wrapper.setProps({
      node: {
        id: '1',
        x: 100,
        y: 200,
        label: 'Node 1',
        hidden: false,
        classes: ['class2'],
        metadata: {
          selected: true,
        },
      }
    });

    const rectShape = wrapper.findComponent(PolygonComponent);
    expect(rectShape.exists()).toBe(true);
    expect(rectShape.attributes('class')).toContain('node');
  });

  it('emits the correct events on drag, click, and mouse interactions', async () => {
    const nodeShape = wrapper.findComponent(CircleComponent);

    // Simulate drag start
    await nodeShape.trigger('dragstart');
    expect(wrapper.emitted().dragstart).toBeTruthy();

    // Simulate drag
    await nodeShape.vm.$emit('drag', '1', 10, 20);
    expect(wrapper.emitted().drag).toBeTruthy();
    expect(wrapper.emitted().drag[0]).toEqual(['1', 10, 20]);

    // Simulate drag end
    await nodeShape.trigger('dragend');
    expect(wrapper.emitted().dragend).toBeTruthy();

    // Simulate left click
    await nodeShape.vm.$emit('leftClickNode', new MouseEvent('click'));
    expect(wrapper.emitted().leftClickEvent).toBeTruthy();
    expect(wrapper.emitted().leftClickEvent[0]).toEqual([expect.any(MouseEvent), node]);

    // Simulate right-click (contextmenu)
    await nodeShape.vm.$emit('contextmenu', new MouseEvent('contextmenu'));
    expect(wrapper.emitted().rightClickEvent).toBeTruthy();

    // Simulate mouseover
    await nodeShape.trigger('mouseover');
    expect(wrapper.emitted().mouseOverEvent).toBeTruthy();

    // Simulate mouseleave
    await nodeShape.trigger('mouseleave');
    expect(wrapper.emitted().mouseLeaveEvent).toBeTruthy();
  });

  it('computes labelX and labelY correctly based on node position and style dimensions', async () => {
    const labelX = wrapper.vm.labelX;
    const labelY = wrapper.vm.labelY;

    expect(labelX).toBe(125);
    expect(labelY).toBe(225);
  });

  it('computes node style correctly using getNodeStyle', () => {
    const nodeStyle = wrapper.vm.nodeStyle;
    expect(nodeStyle.width).toBe(50);
    expect(nodeStyle.height).toBe(50);
    expect(nodeStyle.fill).toBe('red');
  });

  it('renders the selected style correctly when the node is selected', () => {
    const selectedComponent = wrapper.find('.disablePointerEvent');
    expect(selectedComponent.exists()).toBe(true);

    const selectedStyle = wrapper.vm.selectedStyle;
    expect(selectedStyle.fill).toBe('#000000');
    expect(selectedStyle.opacity).toBe(0.4);
  });

  it('hides the node when node.hidden is true', async () => {
    await wrapper.setProps({
      node: { ...node, hidden: true },
    });
    expect(wrapper.find('g').exists()).toBe(false);
  });
});
