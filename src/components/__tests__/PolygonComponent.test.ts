/**
 * @vitest-environment jsdom
 */

import { mount, VueWrapper } from '@vue/test-utils';
import { describe, it, expect, beforeEach } from 'vitest';
import PolygonComponent from '../PolygonComponent.vue';

describe('PolygonComponent.vue', () => {
  let wrapper: VueWrapper;

  const mockNode = {
    id: 'node1',
    x: 100,
    y: 200,
    label: 'Node 1',
  };

  const mockStyle = {
    fill: 'blue',
    stroke: 'black',
    strokeWidth: 2,
    opacity: 0.8,
  };

  const mockShape = {
    points: '50,150 100,100 150,150',
    shape: 'triangle'
  };

  beforeEach(() => {
    wrapper = mount(PolygonComponent, {
      props: {
        node: mockNode,
        style: mockStyle,
        shape: mockShape,
      },
    });
  });

  it('renders the polygon with correct attributes', () => {
    const polygon = wrapper.find('polygon');
    expect(polygon.exists()).toBe(true);
    expect(polygon.attributes('id')).toBe(mockNode.id);
    expect(polygon.attributes('points')).toBe(mockShape.points); // points for polygon shape
    expect(polygon.attributes('fill')).toBe(mockStyle.fill); // fill color
    expect(polygon.attributes('stroke')).toBe(mockStyle.stroke); // stroke color
    expect(polygon.attributes('stroke-width')).toBe(String(mockStyle.strokeWidth)); // stroke width
    expect(polygon.attributes('opacity')).toBe(String(mockStyle.opacity)); // opacity
    expect(polygon.attributes('name')).toBe(mockNode.label); // label
  });

  it('applies cursor style from useDragAndDrop composable', () => {
    const polygon = wrapper.find('polygon');
    expect(polygon.attributes('style')).toBeDefined(); // Check if cursor style is applied
  });
});